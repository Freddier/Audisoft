from AudISoft.extension import FileResponse
from AudISoft.strings import Strings
from apps.indicator.models import IncidenceFile


def download_incidences_file(request, file_id):
    ''' Get incidence excel file from row id '''

    filename, file_data = IncidenceFile.incidence_files(file_id)
    return FileResponse(file_data, filename)


def download_group_file(request, category_id, indicator_id):
    ''' Get incidence excel files from a pattern and download a zip '''

    USER_SETTINGS = request.session.get(Strings.Setting.USER_SETTINGS)
    indicator_type = USER_SETTINGS.get(Strings.Setting.INDICATOR_TYPE)

    filename, file_data = (IncidenceFile.
                           incidence_group_files(category_id,
                                                 indicator_id, indicator_type))
    return FileResponse(file_data, filename)
