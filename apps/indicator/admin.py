from django.contrib import admin

from apps.indicator import models
from apps.security.models import Config


class CategoryInline(admin.StackedInline):

    model = models.Category
#     verbose_name_plural = 'Configuración asignada'
#     verbose_name = 'Configuración'
    fk_name = 'indicator_group'
    extra = 0

    readonly_fields = ["name", "description"]
    list_display = ["name", 'description']

    fieldsets = [
        [ None,
            {
                "fields": [
                    ("name"),
                ]
            }
        ],
    ]


class GroupAdmin(admin.ModelAdmin):

    inlines = (CategoryInline, )

    filter_horizontal = [
        'indicators'
    ]

admin.site.register(models.Group, GroupAdmin)
admin.site.register(models.IncidenceFile)
admin.site.register(models.Category)
admin.site.register(models.Indicator)
