from os import path
from time import localtime, strftime
from tempfile import gettempdir
from zipfile import ZipFile

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.indicator.managers import CategoryManager, IndicatorManager
# import apps.security.models

class BaseModel(models.Model):
    class Meta:
        abstract = True

    def __repr__(self):
        cls_name = self.__class__.__name__
        dict_keys = set(self.__class__.__dict__.keys())
        possible_v = set(('name', 'description', 'indicator', 'office', 'desc',))
        coincidences = (dict_keys & possible_v)
        key = None

        if coincidences:
            key = tuple(coincidences)[0]
            value = getattr(self, key, 'N/A')
        else:
            value = 'N/A'

        return f'{cls_name} <{key}={value}>'


class Category(BaseModel):
    name = models.CharField(max_length=50, null=False)
    description = models.CharField(max_length=600, null=False)
    indicator_group = models.ForeignKey('Group', on_delete=models.CASCADE,
                                        verbose_name='Grupo de indicadores')

    objects = CategoryManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Categoría'


class Indicator(BaseModel):
    code = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=600)
    categories = models.ManyToManyField(Category, through='IndicatorCategory')

    objects = IndicatorManager()

    class Meta:
        verbose_name = 'Indicador'
        verbose_name_plural = '   Indicadores'

    def __str__(self):
        return f'{self.id}-{self.name}'


class Group(BaseModel):
    name = models.CharField(max_length=50, null=False, verbose_name='Nombre')
    description = models.CharField(max_length=600, null=False, verbose_name='Descrición')
    indicators = models.ManyToManyField(Indicator, blank=True, verbose_name='Indicadores')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Grupo'
        verbose_name_plural = ' Grupos'

@receiver(post_save, sender=Group)
def create_group(sender, instance, created, **kwargs):
    from apps.security.models import DefaultConfig, Config
    configs = DefaultConfig.objects.all()

    for default_config in configs:
        config = Config()
        config.config = default_config
        config.value = default_config.value
        config.indicator_group_id = instance.id
        config.save()


class IndicatorType(BaseModel):
    key = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class IndicatorCategory(BaseModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    weight = models.DecimalField(max_digits=5, decimal_places=4)
    indicator = models.ForeignKey(Indicator, on_delete=models.PROTECT)
    indicator_type = models.ForeignKey(IndicatorType, to_field='key', on_delete=models.PROTECT)
    indicator_group = models.ForeignKey(Group, on_delete=models.CASCADE,
                                        verbose_name='Grupo de indicadores')

    def __repr__(self):
        return '(category: "%s", indicator: "%s")' % (self.category.name, self.indicator.name)

    def __str__(self):
        return '(Categoría: "%s", Indicador: "%s")' % (self.category.name, self.indicator.name)

    class Meta:
        verbose_name = 'Categoría de Indicador'


class CategoryWeight(BaseModel):

    weight = models.DecimalField(max_digits=5, decimal_places=4)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    indicator_type = models.ForeignKey(IndicatorType, to_field='key', on_delete=models.PROTECT)
    indicator_group = models.ForeignKey(Group, on_delete=models.CASCADE,
                                        verbose_name='Grupo de indicadores')

    def __str__(self):
        return f'(Categoría="{self.category}", ' \
                'Tipo Indicador="{self.indicator_type}", Peso="{self.weight}")'

    class Meta:
        verbose_name = 'Peso de Categoría'


class IncidenceFile(BaseModel):
    path = models.CharField(max_length=300)
    base_name = models.CharField(max_length=200)
    load_date = models.DateField(max_length=50)
    indicator_code = models.ForeignKey(Indicator, db_column='indicator_code',
                                       to_field='code', on_delete=models.PROTECT)
    population = models.IntegerField()
    involved_amount = models.DecimalField(null=False, max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Archivos de Excepciones'
        verbose_name_plural = 'Archivos de Excepciones'

    def has_many_files(self):
        result = True if len(self.base_name.split(';')) > 1 else False
        return result

    def get_full_path(self):
        if self.has_many_files():
            files = []
            for base_name in self.base_name.split(';'):
                files.append(path.join(self.path, base_name))
            return files
        else:
            return path.join(self.path, self.base_name)

    def get_filename(self):
        return self.base_name

    @staticmethod
    def incidence_group_files(category_id, indicator_id, indicator_type_id):
        # download a group of attached incidence files

        current_time = localtime()
        # add current date/time to filename
        filename = 'Incidencias {}.zip'.format(strftime('%d-%m-%Y %H:%M%p', current_time))
        # get all attached files by indicators
        filelist = list(IncidenceFile.objects.filter(indicator_code__id=indicator_id))
        # save file in a temporary path
        zip_full_name = path.join(gettempdir(),filename)
        # zip file creator instance
        zipper = ZipFile(zip_full_name, 'w')
        # for each incidence file...
        for row in filelist:
            # ... for each semicolon named file ...
            for base_name in row.base_name.split(';'):
            # ... add to the zip file
                zipper.write(path.join(row.path,base_name), base_name)
        # close zipper instance
        zipper.close()

        # read the zipped file
        file_data = open(zip_full_name, 'rb')

        return (filename, file_data)

    @staticmethod
    def incidence_files(file_id):
        ''' Get incidence excel file from row id '''
        incidence_file_record = IncidenceFile.objects.get(pk=file_id)
        if incidence_file_record.has_many_files():
            current_time = localtime()
            # add current date/time to filename
            filename = 'Incidencias %s.zip' % strftime('%d-%m-%Y %H:%M%p', current_time)
            # save file in a temporary path
            zip_full_name = path.join(gettempdir(),filename)
            # zip file creator instance
            zipper = ZipFile(zip_full_name, 'w')
            for file in incidence_file_record.get_full_path():
                zipper.write(file, basename(file))
            zipper.close()

            file_data = open(zip_full_name, 'rb')

            return (filename, file_data)

        else:
            filename = incidence_file_record.get_filename()
            file_data = open(incidence_file_record.get_full_path(), 'rb')
            return (filename, file_data)

    def __str__(self):
        return self.get_filename()
