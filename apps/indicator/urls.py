from django.urls import path

from . import views

urlpatterns = [
    path('file/doc/<int:file_id>/',
         views.download_incidences_file,
         name='download_incidences_file'),

    path('file/zip/<int:category_id>/<int:indicator_id>/',
         views.download_group_file,
         name='download_group_file'),
]
