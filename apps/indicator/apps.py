from django.apps import AppConfig


class IndicatorConfig(AppConfig):
    name = 'apps.indicator'
    verbose_name = 'Indicadores'
