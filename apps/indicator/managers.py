from django.db import models


class CategoryManager(models.Manager):
    def all_dict(self):
        _all = super().get_queryset()
        return [
            {
                'id': category.id,
                'name': category.name
            }
            for category in _all
        ]


class IndicatorManager(models.Manager):

    def by_group(self, group_id):
        _all = super().get_queryset().filter(group=group_id)
        return _all

    def all_dict(self):
        _all = super().get_queryset()
        return [
            {
                'id': indicator.id,
                'name': indicator.code + ' ' + indicator.name
            }
            for indicator in _all
        ]
