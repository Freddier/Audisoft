from datetime import datetime
from mimetypes import guess_type
from ntpath import basename
from os import path, remove as rm_file, getcwd
from json import loads as json_decode
from http import HTTPStatus
from django.http import HttpResponse

from AudISoft.extension import JsonResponse, render, log_exception, FileResponse
from AudISoft.strings import Strings
from apps.indicator.models import IncidenceFile, Indicator, Category
from apps.dashboard.offices.models import Office, Region, IndicatorData


def dashboard(request):
    categories = Category.objects.all_dict()
    indicators = Indicator.objects.all_dict()

    page_data = {
        'categories': categories,
        'indicators': indicators
    }

    return render(request, 'dashboard/index.html', page_data)


def offices_risk(request):
    from apps.dashboard.offices.sql_view_manager import  \
        top_offices, top_categories, group_df_by_index, _TO_RENAME_1, \
        top_indicators, top_indicator, top_category, top_office
    from pandas import DataFrame as df

    settings = request.session[Strings.Setting.USER_SETTINGS]

    group_n = settings['indicator_group_id']
    ENUMBER = settings['considered_executions']
    I_TYPE = settings['indicator_type']

    OFFICE_INDEX = 1
    CATEGORY_INDEX = 2
    INDICATOR_INDEX = 3

    _ = list(_TO_RENAME_1(group_n, ENUMBER))

    if not _:
        return JsonResponse({}, status=HTTPStatus.NO_CONTENT)

    rows = group_df_by_index(df(_))
    _top_offices = group_df_by_index(df(top_offices(group_n, ENUMBER)), OFFICE_INDEX)
    _top_categories = group_df_by_index(df(top_categories(group_n, ENUMBER)), CATEGORY_INDEX)
    _top_indicators = group_df_by_index(df(top_indicators(group_n, ENUMBER)), INDICATOR_INDEX)

    _top_office = top_office(group_n, ENUMBER, I_TYPE)
    _top_category = top_category(group_n, ENUMBER, I_TYPE)
    _top_indicator = top_indicator(group_n, ENUMBER, I_TYPE)

    region_map_metadata = Region.map_metadata(I_TYPE, rows)
    office_map_metadata = Office.map_metadata(rows)

    return JsonResponse({
        'office_map_metadata': office_map_metadata,
        'region_map_metadata': region_map_metadata,
        'rows': {
            'top': {
                'offices': _top_offices,
                'office': _top_office,
                'categories': _top_categories,
                'category': _top_category,
                'indicators': _top_indicators,
                'indicator': _top_indicator
            }
        }
    })


def get_history(request, entity_type, entity_id):
    user_settings = request.session.get(Strings.Setting.USER_SETTINGS)
    indicator_type = user_settings.get(Strings.Setting.INDICATOR_TYPE)

    history_data = OfficeModel().get_history({
        'entity': entity_type,
        'entity_id': entity_id,
        'total_records': 3,
        'indicator_type': indicator_type
    })

    columns = ['Última Ejecución', 'Penultima Ejecución ', 'Antepenultima Ejecución']
    result = {
        'columns': columns[:len(history_data)],
        'values': list(map(lambda x: 0 if len(history_data) == 0 else x['total'], history_data))
    }

    return JsonResponse({'sucess': True, 'history_data': result })

def reports(request):
    data = {
        'offices': Office.objects.all(),
        'categories': Category.objects.all(),
        'indicators': Indicator.objects.all(),
    }

    return render(request,'dashboard/reports.html', data)

def get_execution_info(request):
    form = json_decode(request.body.decode("utf-8"))

    columns = data = []
    group_by = form['group_by']
    group_element = {
        'indicator_id' : {'column': 'indicator_name', 'name': 'Indicador'},
        'category_id' : {'column': 'category_name', 'name': 'Categoría'},
        'office_id' : {'column': 'office_name', 'name': 'Oficina'},
    }

    filter = {
        'offices' : form['offices'],
        'indicators' : form['indicators'],
        'categories' : form['categories'],
        'total_records' : (int)(form['executions_count']),
        'indicator_type' :  form['indicator_type'],
        'group_by' : group_by
    }

    execution_data = OfficeModel().get_executions(filter)
    amount_description = 'Cantidad de incidencias' if filter['indicator_type'] == 'qty' else 'Monto total'

    if group_by in ['indicator_id','category_id','office_id']:
        columns = list(map(lambda c: {'title': c} , [group_element[group_by]['name'], amount_description]))
        data = list(map(lambda d: [d[group_element[group_by]['column']], "{:,}".format(d['total'])], execution_data))
    else:
        columns = list(map(lambda c: {'title': c} , ['Fecha Ejecución', 'Indicador', 'Categoría', 'Oficina', amount_description]))
        data = list(map(lambda d: [d['load_date'], d['indicator_name'], d['category_name'], d['office_name'], "{:,}".format((d['amount'] if filter['indicator_type'] == 2 else (int)(d['qty'])) )], execution_data))

    return {'columns': columns, 'data': data}

def get_executions(request):
    info = get_execution_info(request)
    return JsonResponse({'sucess': True, 'execution_data': {'columns': info['columns'], 'data': info['data']} })

def download_pdf(request):
    path_wkthmltopdf = getcwd() + '\\AudISoft\\library\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    pdfkit.configuration(wkhtmltopdf=path_wkthmltopdf)

    form = json_decode(request.body.decode("utf-8"))
    info = get_execution_info(request)
    template = open('templates/reports/indicators_result.html')

    #Preparing report information
    template_data = []
    columns = list(map(lambda c: '<th>{0}</th>'.format(c['title']), info['columns']))
    for data in info['data']:
        row_data = list(map(lambda d: '<td>{0}</td>'.format(d), data))
        template_data.append("<tr>" + "".join(row_data) + "</tr>")

    #Getting report template
    template_info = template.read()
    template_info = template_info.replace('{base_url}', getcwd())
    template_info = template_info.replace('{report_name}', "" if 'report_name' not in form else form['report_name'])
    template_info = template_info.replace('{columns}', "".join(columns))
    template_info = template_info.replace('{info}', "".join(template_data))
    report_name   = 'Reporte de incidencias {0}.pdf'.format(datetime.now().strftime('%Y-%m-%d %H%M%S'))
    pdfkit.from_string(template_info, "dashboard/static/reports/" + report_name, configuration=config)

    return JsonResponse({'sucess': True, 'report_name': report_name}, safe=False)