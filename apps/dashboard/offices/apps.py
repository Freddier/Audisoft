from django.apps import AppConfig


class OfficesConfig(AppConfig):
    name = 'apps.dashboard.offices'
    verbose_name = 'Dashboard Oficinas'
