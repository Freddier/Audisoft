from django.urls import path

from . import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('reports', views.reports, name='reports'),

    path('api/offices_risk', views.offices_risk, name='offices_risk'),
    path('api/get_executions', views.get_executions, name='get_executions'),
    path('api/download_pdf', views.download_pdf, name='download_pdf'),

    path('api/get_history/<str:entity_type>/<int:entity_id>/',
         views.get_history, name='get_history'),
]
