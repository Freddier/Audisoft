from json import loads as json_decode

from operator import itemgetter

from django.db import models
from apps.indicator.models import BaseModel, Indicator, IncidenceFile

class Region(BaseModel):
    name = models.CharField(max_length=50)
    area = models.TextField(max_length=400000)
    manager = models.CharField(max_length=100)
    center = models.CharField(max_length=100)
    center_zoom = models.IntegerField()
    region_pixel = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Región'
        verbose_name_plural = 'Regiones'

    @staticmethod
    def map_metadata(indicator_type, rows):
        ''' Gives a dict to generate map graph in front-end

        Parameters
        ---------
        rows: array

        Return
        ------
        dict
        '''

        # list of offices filtered by the `ids`
        # of `region_extra_info`
        region_info = Region.objects.all().values()

        result = []

        for region in region_info:

            region_risk_info = [r for r in rows if r.region_id == region['id']]
            risk = 0

            if region_risk_info:
                region_risk_info = region_risk_info[0]
                if indicator_type == 'qty':
                    risk = region_risk_info.region_risk_qty
                else:
                    risk = region_risk_info.region_risk_amount

            area = region['area']

            # the future json which contains the map data
            # (dont worry, will get worse xd)
            region_coord = {
                "type": "Feature",
                "properties": {
                    "id": region['id'],
                    "center": region['center'],
                    "zoom": region['center_zoom'],
                    "pixel": region['region_pixel'],
                    "map_risk": risk, #region['map_risk'],
                    "info": {
                        "region_name": {"label": "Región", "value": region['name']},
                        "risk": {"label": "% Incidencias", "value": 2},      #region['risk']},
                        "manager": {"label": "Encargado", "value": region['manager']}
                    }
                },
                "geometry": {
                    "type": "Polygon",
                    "coordinates": area
                }
            }

            result.append(region_coord)

        return result


class Office(BaseModel):
    code = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    region = models.ForeignKey(Region, on_delete=models.PROTECT)
    location = models.CharField(max_length=500)
    schedule = models.CharField(max_length=500)
    office_type = models.CharField(max_length=4)
    manager = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Oficina'

    def __str__(self):
        return self.name

    @staticmethod
    def map_metadata(rows):
        ''' Gives a dict to generate map graph in front-end

        Parameters
        ---------
        rows: array

        Return
        ------
        dict
        '''

        sorting_key = itemgetter("id")

        # get the keys to complete the map data
        # from the rows list
        # It also has the list of offices to be filtered
        office_extra_info = []; [
            [
                office_extra_info.append(
                    {
                        'id': t.office_id,
                        'risk_qty': t.office_risk_qty,
                        'risk_amount': t.office_risk_amount,
                        'region_name': x.region_name
                    })
                for t in x.offices
            ]
            for x in rows
        ]

        # list of offices filtered by the `ids`
        # of `office_extra_info`
        office_info = (Office.objects
                       .filter(id__in=list(map(lambda x: x.get('id'),
                                               office_extra_info))).values())

        # update each item of office_info with
        # the office extra info.
        # `zip` is to iterate over to list at same time
        for i, j in zip(sorted(office_info, key=sorting_key),
                        sorted(office_extra_info, key=sorting_key)):
            i.update(j)

        result = []

        for office in office_info:

            # `location` is in '##.#####,##.#####' format
            location = office['location'].split(',')

            if not location: continue

            # cast both elements to float
            lat, lng = list(map(float, location))

            # the future json which contains the map data
            # (dont worry, will get worse xd)
            office_coord = {
                "type": "Feature",
                "properties": {
                    "Y": lat,
                    "X": lng,
                    "region": office['region_name'],
                    "info": {
                        "code": {"label": "Código", "value": office['code']},
                        "office_natme": {"label": "Oficina", "value": office['name']},
                        "manager": {"label": "Gerente", "value": office['manager']},
                        "region": {"label": "Región", "value": office['region_name']},
                        "type": {"label": "Tipo", "value": office['office_type']},
                        "address": {"label": "Dirección", "value": office['address']},
                        "schedule": {"label": "Horario", "value": office['schedule']},
                    }
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": [lng, lat]
                }
            }
            result.append(office_coord)

        return result


class IndicatorData(BaseModel):
    qty = models.DecimalField(max_digits=15, decimal_places=0)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    indicator = models.ForeignKey(Indicator, on_delete=models.PROTECT)
    attached_file = models.ForeignKey(IncidenceFile, on_delete=models.PROTECT)
    office = models.ForeignKey(Office, db_column='office_code',
                               to_field='code', on_delete=models.PROTECT)


# class Dashboard():

#     @staticmethod
#     def get_view(view_name, params = None):
#         from AudISoft.extension import dictfetchall
#         query = 'select * from {0}'.format(view_name)
#         if params:
#             query += ' where %s' % (' and '.join(list(map(lambda x: "%s ='%s'" % (x, params[x]), params))))
#         return dictfetchall(query)


class BhkCalculos(models.Model):
    id = models.IntegerField(primary_key=True)

    group_id = models.IntegerField()
    group_name = models.CharField(max_length=50)

    region_id = models.IntegerField()
    region_name = models.CharField(max_length=50)
    region_total_qty = models.IntegerField()
    region_total_amount = models.IntegerField()
    region_risk_qty = models.IntegerField()
    region_risk_amount = models.IntegerField()

    office_id = models.IntegerField()
    office_code = models.CharField(max_length=50)
    office_name = models.CharField(max_length=50)

    office_total_qty = models.IntegerField()
    office_total_amount = models.IntegerField()
    office_risk_amount = models.DecimalField(max_digits=5, decimal_places=4)
    office_risk_qty = models.DecimalField(max_digits=5, decimal_places=4)

    category_id = models.IntegerField()
    category_name = models.CharField(max_length=50)
    category_total_qty = models.DecimalField(max_digits=29, decimal_places=4,
                                             blank=True, null=True)
    category_total_amount = models.DecimalField(max_digits=31, decimal_places=6,
                                                blank=True, null=True)
    category_risk_amount = models.DecimalField(max_digits=5, decimal_places=4)
    category_risk_qty = models.DecimalField(max_digits=5, decimal_places=4)

    indicator_id = models.IntegerField()
    indicator_code = models.CharField(max_length=50)
    indicator_name = models.CharField(max_length=100)
    indicator_percent_qty = models.DecimalField(max_digits=29, decimal_places=4,
                                                blank=True, null=True)
    indicator_percent_amount = models.DecimalField(max_digits=31, decimal_places=6,
                                                   blank=True, null=True)

    indicator_risk_amount = models.DecimalField(max_digits=5, decimal_places=4)
    indicator_risk_qty = models.DecimalField(max_digits=5, decimal_places=4)

    file_id = models.IntegerField()
    file_path = models.CharField(max_length=300)
    file_base_name = models.CharField(max_length=200)
    file_load_date = models.DateField()
    executions_order = models.BigIntegerField(blank=True, null=True)

    def to_dict(self, keys=None):

        _dict = {}

        if keys:
            _d = self.__dict__
            for key in keys:
                _dict[key] = _d[key]

            return _dict

        else:
            return {
                'id': self.id,
                'group_id': self.group_id,
                'group_name': self.group_name,
                'region_id': self.region_id,
                'region_name': self.region_name,
                'office_id': self.office_id,
                'office_code': self.office_code,
                'office_name': self.office_name,
                'office_qty': self.office_qty,
                'office_amount': self.office_amount,
                'category_id': self.category_id,
                'category_name': self.category_name,
                'category_percent_qty': self.category_percent_qty,
                'category_percent_amount': self.category_percent_amount,
                'indicator_id': self.indicator_id,
                'indicator_code': self.indicator_code,
                'indicator_name': self.indicator_name,
                'indicator_percent_qty': self.indicator_percent_qty,
                'indicator_percent_amount': self.indicator_percent_amount,
                'file_id': self.file_id,
                'file_path': self.file_path,
                'file_base_name': self.file_base_name,
                'executions_order': self.executions_order
            }

    def __repr__(self):
        return f'''
                <BHK_CALCULOS>
                region_id='{self.region_id}'
                region_name='{self.region_name}'

                office_id='{self.office_id}'
                office_code='{self.office_code}'
                office_qty='{self.office_qty}'
                office_amount='{self.office_amount}'

                category_id='{self.category_id}'
                category_name='{self.category_name}'
                category_percent_qty='{self.category_percent_qty}'
                category_percent_amount='{self.category_percent_amount}'

                indicator_id='{self.indicator_id}'
                indicator_code='{self.indicator_code}'
                indicator_name='{self.indicator_name}'
                indicator_percent_qty='{self.indicator_percent_qty}'
                indicator_percent_amount='{self.indicator_percent_amount}'

                file_id='{self.file_id}'
                file_path='{self.file_path}'
                file_base_name='{self.file_base_name}'

                executions_order='{self.executions_order}'
                </BHK_CALCULOS>
                '''

    class Meta:
        managed = False
        db_table = 'bhk_calculos'
