from functools import reduce
from collections import namedtuple
from re import match as re_match

from pandas import DataFrame as df

from django.conf import settings
from AudISoft.extension import namedtuplefetchall
from AudISoft.decorators import validate_indicator_type
from apps.dashboard.offices.models import BhkCalculos
# ROOT_VIEW_PATH = 'dashboard\\sql_views\\'


# Habría que analizar bien cuales serían esos días del mes porque resulta sorprendente la cantidad de personas que tienen días definidos por particulares como beneficios por tarjetas de crédito o cosas así.
# Lo que no sorprende es que si estuviera chismeando sobre peleas, recomendado películas o escribiendo 💩 sobre moneyfree, seguro que esto estuviera lleno de comentarios.
# Pero como esto es tan poco trivial y _concierne a otro_, resulta más fácil quejarse, o ignorar.

FIELDS = {
    'region': ['region_id',
               'region_name',
               'region_total_qty',
               'region_total_amount',
               'region_risk_qty',
               'region_risk_amount',
               'offices'],
    'office': ['office_id',
               'office_code',
               'office_name',
               'office_total_qty',
               'office_total_amount',
               'office_risk_qty',
               'office_risk_amount',
               'categories'],
    'category': ['category_id',
                 'category_name',
                 'category_risk_amount',
                 'category_risk_qty',
                 'category_total_amount',
                 'category_total_qty',
                 'indicators'],
    'indicator': ['indicator_id',
                  'indicator_name',
                  'indicator_total_amount',
                  'indicator_total_qty',
                  'indicator_percent_amount',
                  'indicator_percent_qty',
                  'indicator_risk_amount',
                  'indicator_risk_qty',
                  'files'],
    'file': ['file_id',
             'file_path',
             'file_base_name',
             'file_load_date',
             'k']
}

Region = namedtuple('region', FIELDS.get('region'))
Office = namedtuple('office', FIELDS.get('office'))
Category = namedtuple('category', FIELDS.get('category'))
Indicator = namedtuple('indicator', FIELDS.get('indicator'))
File = namedtuple('file', FIELDS.get('file'))

sort_steps = (
    ('region', Region),
    ('office', Office),
    ('category', Category),
    ('indicator', Indicator),
    ('file', File),
)


def _TO_RENAME_1(group_id, execution_number):
    '''
        Execute dashboard_offices_risk stored procedure

        Parameters
        ----------
        execution_number : int
            Numbers of executions to filter

        Returns
        -------
        namedtuple
            Elements array from db
    '''
    try:
        int(execution_number)
    except ValueError:
        raise ValueError('"execution_number" argument must be integer')

    fields = []
    for x in FIELDS:
        for field in FIELDS[x][:-1]:
            fields.append(field)

    f = ', '.join(fields)

    return namedtuplefetchall(f'select {f} from bhk_calculos a'
                              ' where '
                              f' executions_order = {execution_number}'
                              f' and group_id = {group_id}'
                              ' group by a.region_id, '
                              ' a.office_id,'
                              ' a.category_id,'
                              ' a.indicator_id')


def group_df_by_index(df_rows, index=0):
    """
        Obtain cascaded ordered dict with the format:

            region ->
                office ->
                    category ->
                        indicator ->

        Parameters
        ----------
        df_rows : pandas.dataframe
            generated from _TO_RENAME_1

        index : int (optional)
            to start by a position is not the region:
            by instance:

                  index
                    0    region ->
                    1       office ->
                    2           category ->
                    3               indicator ->
                    4                   files

        Returns
        -------
        list
     """

    if index == 5: return []

    fields_name, _namedtuple = sort_steps[index]
    grouped = df_rows.groupby(FIELDS.get(fields_name)[:-1])

    index += 1
    items = []

    for c_val, c_group in grouped:
        # create namedtuple dynamically
        item = _namedtuple(*c_val, [])

        # cause steps are simillar, fill the children
        # values calling recursively this function
        k = group_df_by_index(c_group, index)

        # set the namedtuple children value:
        # region.'offices' => office.'categories' => category.'indicator'
        getattr(item, FIELDS.get(fields_name)[-1]).extend(k)
        items.append(item)

    return items


def top_indicator(group, execution_number, indicator_type='qty'):
  pass

@validate_indicator_type
def top_offices(group, execution_number, indicator_type='qty'):

    order_by = f'office_{indicator_type}'
    v_list = list(FIELDS.values())
    index = 1  # start in Office
    values = reduce(lambda x, c: (x + c[:-1]), v_list[index:], v_list[index][:-1])

    query = BhkCalculos.objects.raw('''
        SELECT *
        FROM bhk_calculos a
        WHERE a.group_id = %s AND a.executions_order = %s
        GROUP BY a.office_id
        ORDER BY %s DESC
    ''', [group, execution_number, order_by])

    return [row.to_dict(values) for row in query]


@validate_indicator_type
def top_categories(group, execution_number, indicator_type='qty'):

    order_by = f'category_total_{indicator_type}'
    v_list = list(FIELDS.values())
    index = 2  # start in Office
    values = reduce(lambda x, c: (x + c[:-1]), v_list[index:], v_list[index][:-1])

    query = BhkCalculos.objects.raw('''
        SELECT *
        FROM bhk_calculos a
        WHERE a.group_id = %s AND a.executions_order = %s
        GROUP BY a.category_id
        ORDER BY %s DESC
    ''', [group, execution_number, order_by])

    return [row.to_dict(values) for row in query]


@validate_indicator_type
def top_indicators(group, execution_number, indicator_type='qty'):

    order_by = f'category_total_{indicator_type}'
    v_list = list(FIELDS.values())
    index = 3
    values = reduce(lambda x, c: (x + c[:-1]), v_list[index:], v_list[index][:-1])

    query = BhkCalculos.objects.raw('''
        SELECT *
        FROM bhk_calculos a
        WHERE a.group_id = %s AND a.executions_order = %s
        GROUP BY a.indicator_id, a.category_id
        ORDER BY %s DESC
    ''', [group, execution_number, order_by])

    return [row.to_dict(values) for row in query]


@validate_indicator_type
def top_indicator(group, execution_number, indicator_type='qty'):

    field_name = f'indicator_risk_{indicator_type}'

    sql = (BhkCalculos.objects
           .filter(group_id=group, executions_order=execution_number)
           .distinct()
           .values('indicator_id', 'indicator_code', 'indicator_name', field_name))

    if not sql: return None

    indicators = df(list(sql))
    greater_indicators = indicators.groupby(['indicator_id',
                                            'indicator_code',
                                            'indicator_name']) \
                                  .sum() \
                                  .reset_index()

    greater_indicators.sort_values(field_name,
                                  ascending=False, inplace=True)

    greater_indicators.rename(columns={field_name: 'risk'},
                             inplace=True)

    greatest_indicator = greater_indicators.iloc[0].to_dict()

    return greatest_indicator


@validate_indicator_type
def top_office(group, execution_number, indicator_type='qty'):

    field_name = f'office_risk_{indicator_type}'
    sql = (BhkCalculos.objects
           .filter(group_id=group, executions_order=execution_number)
           .distinct()
           .values('office_id', 'office_name', field_name))

    if not sql: return None

    offices = df(list(sql))
    greater_offices = offices.groupby(['office_id', 'office_name']) \
                             .sum() \
                             .reset_index()

    greater_offices.sort_values(field_name,
                                ascending=False, inplace=True)

    greater_offices.rename(columns={field_name: 'risk'},
                           inplace=True)

    greatest_office = greater_offices.iloc[0].to_dict()

    return greatest_office


@validate_indicator_type
def top_category(group, execution_number, indicator_type='qty'):

    field_name = f'category_risk_{indicator_type}'
    sql = (BhkCalculos.objects
           .filter(group_id=group, executions_order=execution_number)
           .distinct()
           .values('category_id', 'category_name', field_name))

    if not sql: return None

    offices = df(list(sql))
    greater_categories = offices.groupby(['category_id', 'category_name']) \
                                .sum() \
                                .reset_index()

    greater_categories.sort_values(field_name, ascending=False, inplace=True)

    greater_categories.rename(columns={field_name: 'risk'}, inplace=True)

    greatest_category = greater_categories.iloc[0].to_dict()

    return greatest_category


def tree_risk(_filter):
    '''
        execute risk_information_tree stored procedure
    '''
    query_string = ' 1 = 1 '

    OFFICE_REX = r'^\w{1,5}$'
    DATE_REX = r'^\d{2,4}[-\/\\]\d{2,4}[-\/\\]\d{2,4}$'

    office = _filter.get('office')
    date = _filter.get('date')
    total_records = _filter.get('total_records', settings.TOTAL_RECORDS)

    if date:
        date_from = date.get('from')
        date_to = date.get('to')

    if date and re_match(DATE_REX, date_from):
        # validate input to avoid sqli!!
        query_string += f" AND di.load_date >= '{date_from}' "

    if date and re_match(DATE_REX, date_to):
        # validate input to avoid sqli!!
        query_string += f" AND di.load_date <= '{date_to}' "

    if office and re_match(OFFICE_REX, office):
        # validate input to avoid sqli!!
        query_string += f" AND office_code = {office}"

    return dictfetchall(f'call risk_information_tree({total_records},"{query_string}")')


def execution_history(_filter):
    '''
        Receive a dict with filters to
        return the result of the execution
        of execution_history stored procedure
    '''
    entity = _filter['entity']
    entity_id = _filter['entity_id']
    indicator_type = _filter['indicator_type']
    total_records = _filter.get('total_records', settings.TOTAL_RECORDS)

    query = 'CALL execution_history("{0}", "{1}", {2}, {3});'.format(entity, indicator_type, total_records, entity_id)

    return dictfetchall(query)


def executions_report(_filter):
    record_filter = ''
    group_by = _filter['group_by']
    indicator_type = _filter['indicator_type']
    total_records = _filter.get('total_records', settings.TOTAL_RECORDS)

    offices = _filter['offices']
    indicators = _filter['indicators']
    categories = _filter['categories']

    join_commas = lambda array: ",".join([x for x in array])

    if offices:
        record_filter += ' AND office_id IN(' + join_commas(offices) + ")"

    if indicators:
        record_filter += ' AND indicator_id IN(' + join_commas(indicators) + ")"

    if categories:
        record_filter += ' AND category_id IN(' + join_commas(categories) + ")"

    return dictfetchall(f'CALL executions_report({group_by}, {indicator_type}, {total_records}, {record_filter});')
