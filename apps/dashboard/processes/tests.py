from apps.dashboard.offices.models import Indicator, Config
from django.test import TestCase
from apps.processes.models import Node, NodeIndicator
from .test_data import populate_db

# Create your tests here.

class TestIndicator(TestCase):

	def setUp(self):
		populate_db.populate()

	def test_node_indicator(self):
		node_macro_proceso_1 = Node.objects.get(title='Macro Proceso 1')
		indicators = Node.indicators(node_macro_proceso_1.id)
		self.assertNotEquals(indicators, [])

