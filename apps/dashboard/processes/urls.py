from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='crud_processes'),

    url(r'^nodes$', views.root_nodes, name='root_nodes'),
    url(r'^nodes/(?P<node_id>[0-9]+)$', views.single_node, name='single_node'),
    url(r'^nodes/children/(?P<parent_id>[0-9]+)$', views.children_nodes, name='children_nodes'),
    url(r'^indicators_per_node/(?P<node_id>[0-9]+)$', views.indicators_per_node, name='indicators_per_node'),

    url(r'^execution_by_indicator/(?P<indicator_id>[0-9]+)$', views.execution_by_indicator, name='execution_by_indicator'),
    url(r'^incidences_file/(?P<file_id>[0-9]+)$', views.incidences_file, name='incidences_file'),

    url(r'^top_processes/(?P<node_type>[A-Z_]+)$', views.top_processes, name='top_processes'),
    url(r'^generate_process_risk$', views.generate_process_risk, name='generate_process_risk'),
]
