from django.core import serializers
from django.http import HttpResponseBadRequest
from AudISoft import settings
from AudISoft.extension import render, JsonResponse, \
				 select_fields_from_query, dictfetchall, \
				 FileResponse

from AudISoft.strings import Strings
from apps.dashboard.processes.models import Node, ProcessRisk
from apps.indicator.models import Category, Indicator, IncidenceFile
# Create your views here.

def get_executions_qty(request):
	# Getting considered executions count from config
	user_settings = request.session.get(Strings.Setting.USER_SETTINGS)
	return int(user_settings.get(Strings.Setting.EXECUTIONS_QTY))


def index(request):
	categories = Category.objects.all()
	indicators = Indicator.objects.all()
	root_nodes = Node.root()

	page_data = {
		'categories': categories,
		'indicators': indicators,
		'root_nodes': root_nodes
	}

	return render(request,'processes/index.html', page_data)


def single_node(request, node_id):
	node = Node.objects.get(pk=node_id)
	executions_qty = get_executions_qty(request)

	node.risk = Node.risk_as_an_dict(node, executions_qty)
	dict_node = node.to_dict()
	return JsonResponse({
		'node' : dict_node
	})


def root_nodes(request):
	root_nodes = Node.root()
	# get dict representation of every node
	# for json serialization
	dict_nodes = list(map(lambda node: node.to_dict(), root_nodes))

	return JsonResponse({
		'root_nodes' : dict_nodes
	})


def children_nodes(request, parent_id):
	children_nodes = Node.objects.filter(parent_id=parent_id)
	executions_qty = get_executions_qty(request)

	def add_risk(_node):
		_node.risk = Node.risk_as_an_dict(_node, executions_qty)
		return _node.to_dict()

	dict_children =  sorted([
		add_risk(node) 
		for node in children_nodes
	], key=lambda x: x['is_enabled'], reverse=True)

	return JsonResponse({
		'children_nodes' : dict_children
	})


def indicators_per_node(request, node_id):
	node = Node.objects.get(pk=node_id)
	indicators = Node.indicators(node)

	return JsonResponse(
		{'indicators':indicators}
	)


def incidences_file(request, file_id):
	# Get incidence excel file from row id
	filename, file_data = IncidenceFile.incidence_files(file_id)
	return FileResponse(file_data, filename)


def execution_by_indicator(request, indicator_id):
	indicator_exists = Indicator.objects.filter(pk=indicator_id).exists()

	if indicator_exists:
		USER_SETTINGS = request.session.get(Strings.Setting.USER_SETTINGS)
		considered_executions = USER_SETTINGS.get('considered_executions')
		query = f'CALL indicators_executions_for_processes({considered_executions}, "indicator_id = {indicator_id}")'

		executions = dictfetchall(query)
	else:
		return HttpResponseBadRequest('El indicador proporcionado no existe')

	return JsonResponse({
		'executions' : executions
	})


def top_processes(request, node_type):
	USER_SETTINGS = request.session.get(Strings.Setting.USER_SETTINGS)
	INDICATOR_TYPE = USER_SETTINGS[Strings.Setting.INDICATOR_TYPE]
	executions_qty = get_executions_qty(request)

	order_pattern = f'-{INDICATOR_TYPE}_risk'

	process_risks = [ process for process in
		ProcessRisk.objects
				.filter(executions_qty=executions_qty, node__node_type=node_type)
				.order_by(order_pattern)[:5]
				.select_related('node')
	]

	return JsonResponse(
		{'process_risks':process_risks}
	)


def generate_process_risk(request):
	#Calculating processes risk
	success = True
	for execution_number in range(1, settings.EXECUTIONS_QTY + 1):
		if not Node.get_all_processes_risk(execution_number).insert_data():
			success = False
			break;

	return JsonResponse(
		{'success': success}
	)
