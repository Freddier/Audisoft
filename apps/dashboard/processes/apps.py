from django.apps import AppConfig


class ProcessesConfig(AppConfig):
    name = 'apps.dashboard.processes'
    verbose_name = 'Dashboard Procesos'
