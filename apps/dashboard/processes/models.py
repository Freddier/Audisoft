from datetime import datetime
from itertools import groupby
from operator import itemgetter

from django.db import models
from django.db import transaction

from AudISoft.extension import EnumField
from apps.indicator.models import Indicator


class Node(models.Model):

    CATEGORY = 'CATEGORY'
    MAC_PROC = 'MAC_PROC'
    MID_PROC = 'MID_PROC'
    SUB_PROC = 'SUB_PROC'

    NODE_TYPES = (
        (CATEGORY, 'Categoria'),
        (MAC_PROC, 'Macro Proceso'),
        (MID_PROC, 'Proceso'),
        (SUB_PROC, 'Sub Proceso'),
    )

    title = models.CharField(max_length=100, null=False)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)
    is_enabled = models.BooleanField()
    node_type = EnumField(choices=NODE_TYPES, default=MID_PROC)

    # informative fields
    description = models.TextField(null=False)
    leader_name = models.CharField(max_length=60, null=False)
    leader_position = models.CharField(max_length=60, null=False)
    owner_name = models.CharField(max_length=60, null=False)
    owner_position = models.CharField(max_length=60, null=False)

    risk = None
    # this node will contain the risk when calling
    # 'risk_level_from_executions' method

    class Meta:
        verbose_name = 'Nodo'
        verbose_name_plural = 'Nodos'

    def children(self):
        # give all node children
        return self.node_set.all()

    def to_dict(self):
        return {
            'id': self.id,

            'title': self.title,
            'parent': self.parent_id,
            'is_enabled': self.is_enabled,
            'node_type': self.node_type,

            'description': self.description,
            'leader_name': self.leader_name,
            'leader_position': self.leader_position,
            'owner_name': self.owner_name,
            'owner_position': self.owner_position,

            'number_of_children': self.children().count(),

            'risk': self.risk
        }

    @staticmethod
    def risk_as_an_dict(node, executions_qty):
        # In special cases we have to return an array of risks,
        # however, for avoiding to write same comprobation
        # this one - - - >   risk[0] if len(risk) > 0 else None
        # when risk needed, this function return an dict
        risk = Node.risk_level_from_executions(node, executions_qty)
        return risk[0] if risk else None

    @staticmethod
    def risk_level_from_executions(node, executions_qty):
        get_data = lambda x: {
            'qty_risk': x.qty_risk,
            'amount_risk': x.amount_risk,
            'qty_sum': x.qty_sum,
            'amount_sum': x.amount_sum,
            'executions_qty': x.executions_qty
        }

        # Looking for process risk of execution
        if executions_qty > 0:
            executions = (node.processrisk_set
                          .filter(executions_qty__lte=executions_qty)
                          .all()
                          .order_by('-executions_qty'))
        else:
            executions = node.processrisk_set.all()

        return [get_data(x) for x in executions]

    @staticmethod
    def get_all_processes_risk(executions_qty):
        return ProcessRiskCalculator(executions_qty).calculate_processes_risk()

    @staticmethod
    def indicators(node):
        indicator_list = []
        get_indicator = lambda x: [
            {
                'id': d.indicator.id,
                'name': d.indicator.name,
                'code': d.indicator.code
            } for d in x
        ]

        # Getting all nodes indicator_list
        indicator_list = indicator_list + get_indicator(node.nodeindicator_set.all())

        if node.node_type == Node.MAC_PROC:
            for node in node.node_set.all():
                # Getting all nodes indicator_list
                indicator_list = indicator_list + get_indicator(node.nodeindicator_set.all())

        indicator_list.sort(key=itemgetter('id'))
        indicators = [id[0] for id in groupby(indicator_list)]

        return indicators

    @staticmethod
    def root():
        # we want root nodes, so parent_id == None
        return Node.objects.filter(parent_id=None)

    def __str__(self):
        return f'{self.id}-{self.title}'

    def __unicode__(self):
        return self.title


class ProcessRisk(models.Model):
    node = models.ForeignKey(Node, on_delete=models.PROTECT)
    qty_risk = models.DecimalField(null=False, max_digits=10, decimal_places=2)
    amount_risk = models.DecimalField(null=False, max_digits=10, decimal_places=2)
    qty_sum = models.IntegerField()
    amount_sum = models.DecimalField(null=False, max_digits=10, decimal_places=2)
    executions_qty = models.IntegerField()
    last_update = models.DateTimeField(max_length=50)

    def __str__(self):
        return f"Nodo: {self.node.title} - cantidad de incidencias: {self.qty_risk}"


class NodeIndicator(models.Model):
    class Meta:
        unique_together = ('indicator', 'node')

    node = models.ForeignKey(Node, on_delete=models.PROTECT)
    indicator = models.ForeignKey(Indicator, on_delete=models.PROTECT)
    qty = models.DecimalField(null=False, max_digits=10, decimal_places=2)
    amount = models.DecimalField(null=False, max_digits=10, decimal_places=2)

    def __unicode__(self):
        return ('NodeIndicator(node="{0}",indicator="{1}")'
                .format(self.node.title, self.indicator.name))

    def __str__(self):
        return self.__unicode__()


class ProcessRiskCalculator(object):

    def __init__(self, executions_qty):
        self.main_process_hierarchy = Node.objects.filter(parent_id=None)
        self.executions_qty = executions_qty
        self.indicators_executions_risk = {}
        self.processes_risk = []

    def get_executions(self):
        indicators_executions = {}
        # Importing extension for calling db SQL
        from AudISoft.extension import dictfetchall
        # Getting executions information
        executions = dictfetchall('CALL indicators_executions_for_processes({0},"")'
                                  .format(self.executions_qty))
        # Grouping by indicator and sum qty and amount
        for execution in executions:
            if execution['executions_order'] != self.executions_qty:
                continue

            if not execution['indicator_id'] in indicators_executions:
                # Adding indicator to the list
                indicators_executions[execution['indicator_id']] = []

            # Avoid to save other exections
            indicators_executions[execution['indicator_id']].append(execution)

        return indicators_executions


    def __get_indicators_executions(self, indicators_executions):
        indicators_executions_risk = {}

        for indicators_execution in indicators_executions:
            indicator_info = indicators_executions[indicators_execution]

            qty_risk = amount_risk = total_qty = total_amount = 0
            total_records = len(indicator_info)
            for data in indicator_info:
                #Risk calculation for total incidences and population
                qty_risk += (data['sum_qty'] / data['population'] if data['population'] > 0 else 0)
                total_qty += data['sum_qty']
                #Risk calculation for amount of incidences comparing to involved_amount
                amount_risk += (data['sum_amount'] / data['involved_amount'] if data['involved_amount'] > 0 else 0)
                total_amount += data['sum_amount']

            indicators_executions_risk[indicators_execution] = {
                'qty_risk':qty_risk / total_records,
                'amount_risk': amount_risk / total_records,
                'sum_qty': total_qty,
                'sum_amount': total_amount
            }

        return indicators_executions_risk

    def calculate_processes_risk(self):
        #Looking for indicator execution risk
        executions = self.get_executions()
        self.indicators_executions_risk = self.__get_indicators_executions(executions)
        for process_category in self.main_process_hierarchy:
            self.__get_macro_processes_risk(process_category.node_set.all())

        return self

    def __get_macro_processes_risk(self, macro_processes):
        for macro_process in macro_processes:
            processes = self.__get_process_risk(macro_process.node_set.all())
            qty_risk_sum = amount_risk_sum = qty_sum = amount_sum = 0
            processes_qty = len(processes)
            #Not processing information if macroprocess has no process
            if processes_qty < 1:
                continue

            #Calculating macro-process risk
            for process in processes:
                qty_risk_sum += process['qty_risk']
                amount_risk_sum += process['amount_risk']
                qty_sum += process['qty_sum']
                amount_sum += process['amount_sum']

            #Adding macro-process to the list that will be inserted
            self.processes_risk.append({
                'node_id': macro_process.id,
                'qty_risk': qty_risk_sum / processes_qty,
                'amount_risk': amount_risk_sum / processes_qty,
                'qty_sum': qty_sum,
                'amount_sum': amount_sum
            })      

    def __get_process_risk(self, processes):
        process_list = []
        for process in processes:
            indicator_info = self.__get_indicators_risk(process.nodeindicator_set.all())

            #Adding process to the list that will be inserted
            process_data = {
                'node_id': process.id,
                'qty_risk': indicator_info['sum_qty_risk'],
                'amount_risk': indicator_info['sum_amount_risk'],
                'qty_sum': indicator_info['sum_qty'],
                'amount_sum': indicator_info['sum_amount']
            }
            #Saving processes for calcularting macro-process risk
            process_list.append(process_data)
            self.processes_risk.append(process_data)
            self.__set_subprocess_risk(process.node_set.all())

        return process_list

    def __set_subprocess_risk(self, subprocesses):
        for subprocess in subprocesses:
            indicator_info = self.__get_indicators_risk(subprocess.nodeindicator_set.all())

            #Adding subprocess to the list that will be inserted
            self.processes_risk.append({
                'node_id': subprocess.id,
                'qty_risk': indicator_info['sum_qty_risk'],
                'amount_risk': indicator_info['sum_amount_risk'],
                'qty_sum': indicator_info['sum_qty'],
                'amount_sum': indicator_info['sum_amount']
            })

    def __get_indicators_risk(self, indicators_list):
        sum_qty_risk = sum_amount_risk = sum_qty = sum_amount = 0

        #Looking for risk of indicators asociated to the process
        for indicator in indicators_list:
            #If indicator has no executions, then process will not process this indicator
            if not indicator.indicator_id in self.indicators_executions_risk:
                continue

            indicator_qty_weight    = indicator.qty
            indicator_amount_weight = indicator.amount
            #Getting indicator risk on executions
            indicator_execution_risk = self.indicators_executions_risk[indicator.indicator_id]
            #Indicator risk evaluation, considering the number of executions
            qty_risk = (indicator_qty_weight * indicator_execution_risk['qty_risk'])
            #Indicator risk evaluation, considering the amount of executions
            amount_risk = (indicator_amount_weight * indicator_execution_risk['amount_risk'])

            #Totalizing results
            sum_qty_risk += qty_risk
            sum_amount_risk = sum_amount_risk + amount_risk
            #Getting the total
            sum_qty += indicator_execution_risk['sum_qty']
            sum_amount += indicator_execution_risk['sum_amount']

        return {
            'sum_qty_risk': sum_qty_risk,
            'sum_amount_risk': sum_amount_risk,
            'sum_qty': sum_qty,
            'sum_amount': sum_amount
        }

    def show_calculation(self):
        print(self.processes_risk)

    def show_calculation_with_values(self):
        for node in self.processes_risk:
            if(node['qty_risk'] > 0 or node['amount_risk'] > 0):
                print(node)

    def insert_data(self):
        from AudISoft.extension import log_exception

        try:
            with transaction.atomic():
                #Cleaning the db before inserting data
                ProcessRisk.objects.filter(executions_qty = self.executions_qty).delete()

                for node in self.processes_risk:
                    if(node['qty_risk'] > 0 or node['amount_risk'] > 0):
                        process_risk = ProcessRisk()
                        process_risk.qty_risk = round(node['qty_risk'], 2)
                        process_risk.amount_risk = round(node['amount_risk'], 2)
                        process_risk.qty_sum = round(node['qty_sum'], 2)
                        process_risk.amount_sum = round(node['amount_sum'], 2)
                        process_risk.executions_qty = self.executions_qty
                        process_risk.last_update = datetime.now()
                        process_risk.node_id = node['node_id']
                        process_risk.save()
        except Exception as e:
            log_exception(__name__, e, 'Failed inserting process risk')
            return False

        return True