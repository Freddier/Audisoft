from django.db import transaction
from django.db.models import Model, CharField, \
                            PROTECT, ForeignKey
from json import loads as json_decode
from AudISoft.extension import execute_sql, dictfetchall, log_exception
from apps.indicator.models import Category, IndicatorCategory, CategoryWeight


class Response:
    # Define class for fowarding the result to json response action

    def __init__(self, success=True, content=None):
        self.success = success
        self.content = content

    def __repr__(self):
        return f'Response(success={self.success})'


class CategoryCRUD:
    # Manage all category CRUD operations

    @staticmethod
    def delete_category(group_id, category_id):
        # Delete category and children

        response = Response()
        try:
            with transaction.atomic():
                # delete the category weigth relationship, ...
                CategoryWeight.objects.filter(indicator_group_id=group_id,
                                              category_id=category_id).delete()
                # ... the relationship between category and indicators
                IndicatorCategory.objects.filter(indicator_group_id=group_id,
                                                 category_id=category_id).delete()
                # finally, delete the category
                Category.objects.filter(id=category_id).delete()

        except Exception as e:
            log_exception(__name__, e, 'Fail to delete category [id:{}]'.format(category_id))
            response.success = False

        return response

    @staticmethod
    def delete_indicator_from_category(request):
        # delete indicator from category
        response = Response()

        try:
            request_data = json_decode(request.body)
            IndicatorCategory.objects.filter(indicator_id=request_data['indicator_id'],
                                             category_id=request_data['category_id']).delete()
        except Exception as e:
            log_exception(__name__, e, 'Fail to delete indicator from category [id:{}]'
                                            .format(category_id))
            response.success = False

        return response

    @staticmethod
    def add_indicator_to_category(group_id, category_id, indicator_id):
        # add indicators to Category
        response = Response()

        try:
            for i in ['qty', 'amount']:
                indicatorcategory = IndicatorCategory()
                indicatorcategory.weight = 0
                indicatorcategory.indicator_type_id = i
                indicatorcategory.indicator_group_id = group_id
                indicatorcategory.indicator_id = indicator_id
                indicatorcategory.category_id = category_id
                indicatorcategory.save()

            # return added indicator id
            # response.content = {'id': indicatorcategory.id}

        except Exception as e:
            response.success = False
            log_exception(__name__, e, 'Fail to add indicator [id:{}] to category [id:{}] ' \
                            .format(indicator_id, category_id))

        return response

    @staticmethod
    def create_category(name, description, group_id):
        # create new category
        response = Response()

        try:
            with transaction.atomic():
                category = Category()
                category.name = name
                category.description = description
                category.save()
                response.content = category.id

                for i in ['qty', 'amount']:
                    cw = CategoryWeight()
                    cw.weight = 0
                    cw.indicator_group_id = group_id
                    cw.category = category
                    cw.indicator_type_id = i
                    cw.save()

        except Exception as e:
            log_exception(__name__, e, 'Failed creating category')
            response.success = False

        return response

    @staticmethod
    def save_categoryweight(request):
        response = Response()

        try:
            json_categoryweight = json_decode(request.body)

            sql = ''
            for row in json_categoryweight:
                sql += 'update indicator_categoryweight set weight = {qty} where indicator_type_id = "qty" and category_id = {id}; \n' .format(**row)
                sql += 'update indicator_categoryweight set weight = {amount} where indicator_type_id = "amount" and category_id = {id}; \n\n' .format(**row)
            execute_sql(sql)
        except Exception as e:
            log_exception(__name__, e, 'Fail to save category weights')
            response.success = False

        return response

    @staticmethod
    def save_indicatorcategory(group_id, category, indicators):
        response = Response()

        try:

            # remove all previous rows
            with transaction.atomic():
                IndicatorCategory.objects.filter(category_id=category['id']).delete()
                for indicator in indicators:
                    indicatorcategory = IndicatorCategory()
                    indicatorcategory.indicator_id = indicator['id']
                    indicatorcategory.weight = indicator['qty']
                    indicatorcategory.indicator_type_id = 'qty'
                    indicatorcategory.indicator_group_id = group_id
                    indicatorcategory.category_id = category['id']
                    indicatorcategory.save()

                    indicatorcategory = IndicatorCategory()
                    indicatorcategory.indicator_id = indicator['id']
                    indicatorcategory.weight = indicator['amount']
                    indicatorcategory.indicator_type_id = 'amount'
                    indicatorcategory.indicator_group_id = group_id
                    indicatorcategory.category_id = category['id']
                    indicatorcategory.save()

        except Exception as e:
            log_exception(__name__, e, 'Failed to save category indicators')
            response.success = False

        return response

    @staticmethod
    def get_categories_weight(group_id):
        categories = list(dictfetchall(CategoryCRUD.Queries
                                       .CATEGORY_WITH_BOTH_QTY_AMOUNT
                                       .format(group_id=group_id)))
        return Response(content=categories)

    @staticmethod
    def indicators_in_category(group_id, category_id):
        indicator_category = list(dictfetchall(CategoryCRUD.Queries
                                               .INDICATOR_IN_CATEGORY
                                               .format(group_id=group_id,
                                                       category_id=category_id)))
        return Response(content=indicator_category)

    class Queries():

        INDICATOR_IN_CATEGORY = '''

            select DISTINCT a.category_id, a.indicator_id as id, a.weight as qty,
                    c.code, b.weight amount, c.name, c.description
            from indicator_indicatorcategory a
            join indicator_indicatorcategory b on a.category_id = b.category_id
                                                  and a.indicator_id = b.indicator_id
                                                  and b.indicator_type_id = 'amount'
                                                  and a.indicator_group_id = b.indicator_group_id
            join indicator_indicator c on a.indicator_id = c.id
            where a.indicator_type_id = 'qty'
            and b.indicator_group_id = {group_id} and a.category_id = {category_id} '''

        CATEGORY_WITH_BOTH_QTY_AMOUNT = '''

            select a.category_id as id, a.weight as qty, b.weight as amount , c.name, c.description
            from indicator_categoryweight a
            join indicator_categoryweight b on a.category_id = b.category_id and b.indicator_type_id = 'amount' and a.indicator_group_id = b.indicator_group_id
            join indicator_category c on a.category_id = c.id
            where a.indicator_type_id = 'qty'  and b.indicator_group_id = {group_id}'''
