from django.views.decorators.csrf import csrf_exempt
from http import HTTPStatus
from json import loads as json_decode

from AudISoft.extension import JsonResponse, dictfetchall, \
                                select_fields_from_query, render
from AudISoft.strings import Strings
from apps.indicator.models import Category, IndicatorCategory, Indicator, CategoryWeight
from apps.cruds.models import CategoryCRUD, Response


# define views
def index(request):
    return render(request, 'cruds/index.html', {})


def category_indicator(request):
    return render(request, 'cruds/category_indicator.html')


def answer_request(response: Response):
    success_response = JsonResponse(response.content, safe=False, status=HTTPStatus.OK)
    fail_response = JsonResponse(Strings.Response.GENERAL_BAD_RESPONSE, safe=False,
                                 status=HTTPStatus.INTERNAL_SERVER_ERROR)
    return success_response if response.success else fail_response


def delete_category(request, category_id):

    settings = request.session[Strings.Setting.USER_SETTINGS]
    group_n = settings['indicator_group_id']

    response = CategoryCRUD.delete_category(group_n, category_id)
    return answer_request(response)


def delete_indicator_from_category(request):
    response = CategoryCRUD.delete_indicator_from_category(request)
    return answer_request(response)


def add_indicator_to_category(request):

    settings = request.session[Strings.Setting.USER_SETTINGS]
    request_data = json_decode(request.body)

    category_id = request_data['category_id']
    indicator_id = request_data['indicator_id']
    group_n = settings['indicator_group_id']

    response = CategoryCRUD.add_indicator_to_category(group_n, category_id,
                                                      indicator_id)

    return answer_request(response)


def create_category(request):

    settings = request.session[Strings.Setting.USER_SETTINGS]
    group_n = settings['indicator_group_id']
    json_data = json_decode(request.body)

    response = CategoryCRUD.create_category(json_data['name'],
                                            json_data['description'],
                                            group_n)
    return answer_request(response)


def save_indicatorcategory(request):
    settings = request.session[Strings.Setting.USER_SETTINGS]
    request_data = json_decode(request.body)
    group_n = settings['indicator_group_id']

    category = request_data['category']
    indicators = request_data['indicators']

    response = CategoryCRUD.save_indicatorcategory(group_n, category, indicators)
    return answer_request(response)


def json_categories_weight(request):

    settings = request.session[Strings.Setting.USER_SETTINGS]
    group_n = settings['indicator_group_id']

    response = CategoryCRUD.get_categories_weight(group_n)
    return answer_request(response)


def json_indicator_in_category(request, category_id):

    settings = request.session[Strings.Setting.USER_SETTINGS]
    group_n = settings['indicator_group_id']

    response = CategoryCRUD.indicators_in_category(group_n, category_id)
    return answer_request(response)


def save_categoryweight(request):
    response = CategoryCRUD.save_categoryweight(request)
    return answer_request(response)


def json_indicators(request):
    list_indicator = list(Indicator.objects.all().values())
    return JsonResponse(list_indicator, safe=False)
