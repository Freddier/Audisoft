
from django.urls import path

from . import views

urlpatterns = [
    path('api/change_config', views.change_config, name='change_config'),
]
