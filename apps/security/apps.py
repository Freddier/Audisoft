from django.apps import AppConfig


class SecurityConfig(AppConfig):
    name = 'apps.security'
    app_label = 'security'
    verbose_name = 'Seguridad'
