from json import loads as json_decode
from http import HTTPStatus
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login as _login, logout as _logout
from django.contrib import messages

from AudISoft.decorators import mainpage_if_logged
from AudISoft.extension import JsonResponse
from AudISoft.strings import Strings
from apps.security.auth_backend import AuthenticationBackend, Config


def logout(request):
    _logout(request)
    return redirect('login')


@mainpage_if_logged
def login(request):
    context = {}
    if request.method == 'POST':

        form = request.POST
        user = authenticate(username=form['username'],
                            password=form['password'])
        if user is None:
            messages.error(request, Strings.Session.BAD_LOGIN,
                           extra_tags=Strings.MessageType.NOTIFICATION)
        else:
            _login(request, user)
            AuthenticationBackend.set_default_session(request)
            return redirect('dashboard')

    return render(request, 'security/login.html', context)


def change_config(request):

    if request.method != 'POST':
        return JsonResponse({'fail': 'Method not allowed'}, status=HTTPStatus.FORBIDDEN)

    session_settings = request.session[Strings.Setting.USER_SETTINGS]

    dict_settings = json_decode(request.body.decode("utf-8"))
    user_indicator_group = session_settings['indicator_group_id']

    Config.update_config(user_indicator_group, dict_settings)

    config = Config.get_config(user_indicator_group)
    request.session[Strings.Setting.USER_SETTINGS] = config

    return JsonResponse({'sucess': '!'})
