from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend
from ldap3 import Server, Connection

from AudISoft.strings import Strings
from apps.security.models import Config


class AuthenticationBackend(ModelBackend):
    '''Define custom authentication validating user with active directory'''

    @staticmethod
    def validate_ldap(username, password):
        # Perform login with ldap

        try:
            server = Server('ldap://reservas.BRRD.com')
            connection = Connection(server, user=r'brrd\%s' % username,
                                    password=password)
            connection.open()
            return connection.bind()
        except Exception as ex:
            raise Exception('El servicio de ldap puede no estar funcionando adecuadamente.')

    def authenticate(self, username=None, password=None):
        user = User.objects.get(username=username)

        # TODO enable ldap authentication
        # if self.validate_ldap(username, password):
        if user:
            return user
        else:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    @staticmethod
    def set_default_session(request, indicator_group=None):

        if not indicator_group:
            indicator_group = request.user.userprofile.indicator_group_id

        config = Config.get_config(indicator_group)

        request.session[Strings.Setting.USER_SETTINGS] = config
