from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission, User
from django.contrib import admin

from apps.security.models import UserProfile, DefaultConfig


class ProfileInline(admin.StackedInline):
    model = UserProfile
    verbose_name = 'Perfil de Usuario'
    verbose_name_plural = 'Perfiles de Usuario'
    extra = 0


class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'userprofile')

    inlines = (ProfileInline, )
    can_add = True


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Permission)
admin.site.register(DefaultConfig)
