import json

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import transaction
from django.db.models import Q

from apps.indicator.models import Group


class DefaultConfig(models.Model):
    key = models.CharField(max_length=50)
    description = models.CharField(max_length=200, verbose_name='Descripción')
    value = models.CharField(max_length=20, verbose_name='Valor')

    def __str__(self):
        return f'(key="{self.key}", description="{self.description}")'

    class Meta:
        verbose_name = 'Configración'
        verbose_name_plural = 'Configraciones'


class Config(models.Model):

    config = models.ForeignKey(DefaultConfig, verbose_name='Configuración',
                               related_name='config',
                               on_delete=models.PROTECT)
    value = models.CharField(max_length=20, verbose_name='Valor')
    indicator_group = models.ForeignKey(Group, on_delete=models.CASCADE,
                                        related_name='indicator_groups')

    def __str__(self):
        return f'Configuración (Grupo="{self.indicator_group.name}")'

    class Meta:
        verbose_name = 'Configuración de Grupo'

    @staticmethod
    def get_config(group_id):
        '''
            Retrieve group config.

            If group does not exists or missing some
            settings, default value is given
        '''
        return StoredConfig(group_id).__dict__

    @staticmethod
    def update_config(group_id, dict_settings):

        try:
            with transaction.atomic():

                for key in dict_settings.keys():

                    value = dict_settings[key]
                    Config.objects.filter(indicator_group_id=group_id, config__key=key) \
                                  .update(value=value)

            return dict_settings

        except Exception as e:
            raise Exception('Invalid settings detected:'
                            f'\n "{dict_settings}"')


class UserProfile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    indicator_group = models.ForeignKey(Group, on_delete=models.CASCADE, null=True,
                                        related_name='indicator_group',
                                        verbose_name='Grupo de indicadores')

    def __str__(self):
        return f'(Usuario="{self.user.username}", Grupo="{self.indicator_group}")'

    def __repr__(self):
        return f'UserProfile (user={self.user.username})'

    class Meta:
        app_label = 'security'
        verbose_name = 'Perfil de Usuario'


class StoredConfig():

    def __init__(self, group_id):

        REQUIRED_KEYS = ['indicator_type', 'risk_percent', 'considered_executions']

        config_entries = (DefaultConfig.objects
                          .filter(Q(config__indicator_group_id=group_id)
                                  | Q(config__indicator_group_id=None))
                          .prefetch_related('config')
                          .extra(select={
                              'default_value': 'security_defaultconfig.value',
                              'value': 'security_config.value'
                          })
                          .values('key', 'value', 'default_value'))

        dict_config = StoredConfig.__put_keys_together(config_entries)

        for key in REQUIRED_KEYS:

            if key not in dict_config:
                raise Exception(f'Current group has missing "{key}" key')

            value = dict_config[key]
            if key == 'risk_percent':
                value = StoredConfig.__risk_percent_to_list(value)

            setattr(self, key, value)

        setattr(self, 'indicator_group_id', group_id)

    @staticmethod
    def __risk_percent_to_list(risk_percent):
        return json.loads(risk_percent)

    @staticmethod
    def __put_keys_together(keys):

        _dict = {}
        for key in keys:
            _dict[key['key']] = key['value'] if key['value'] else key['default_value']

        return _dict
