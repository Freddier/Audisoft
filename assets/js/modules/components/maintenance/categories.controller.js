angular.module('MaintenanceCategoriesModules', ['MaintenanceCategoriesServices'])


.controller('MaintenanceCategoriesController', ['$scope','MaintenanceCategories.ApiService', '$timeout', '$q' ,function ($scope, ApiService, $timeout, $q) {

	var WATCHABLES_VAR = ['categories', 'current_indicators'];
	$scope.total = { indicators : { qty : 0, amount : 0}, categories : { qty : 0, amount : 0}};
	$scope.backup = {};
	$scope.current_indicators = [];
	$scope.current_category = null;
	$scope.loading = false;
	$scope.uncompleteTotal = true;

	$scope.addIndicator = function () {


		// take all the indicators that are not already loaded
		var inputOptions = {};

		var optionsArray = $scope.indicators.filter(function(ind) {
			return $scope.current_indicators.map(function(curr) {
				return curr.id
			}).indexOf(ind.id) == -1 
		}).forEach(function(item) {
			inputOptions[item.id] = item.name
		})

		var handleSuccess = function (answer) {

			if (!answer.value) return;
			var indicator_id = answer.value;

		  	var data = {
		  		category_id : $scope.current_category.id,
		  		indicator_id : indicator_id
		  	};

		  	var handleSuccessAdd = function (result) {
				var indicator = $scope.indicators.filter(function (item) {
					return item.id == indicator_id
				})[0];

				indicator.qty = 0;
				indicator.amount = 0;

				swal({
					type: 'success',
					html: 'Se agregó correctamente: ' + indicator.name
				});

				$scope.current_indicators.push(indicator);
		  	}

		  	var handleErrorAdd = function (error) {
				swal("El indicador no pudo ser agregado", error.message,'error');
		  	}
		  	ApiService.post_add_indicator_to_category(data)
				.then(handleSuccessAdd)
				.catch(handleErrorAdd);
		}


		swal({
			title: 'Seleccione el indicador',
			input: 'select',
			inputOptions: inputOptions,
			inputPlaceholder: 'Indicador',
			showCancelButton: true,
		    cancelButtonText: 'Cancelar',
			inputValidator: function (value) {
				return new Promise(function (resolve, reject) {
					if (value) {
						resolve()
					} else {
						reject('Debes seleccionar un indicador');
					}
				})
			}
		}).then(handleSuccess);

	}

	$scope.removeIndicator = function (indicator, $index) {

		var handleSwalActions = function (answer) {

			if (!answer.value) return;

			var params = { indicator_id : indicator.id , category_id : $scope.current_category.id}

			var handleSuccessDelete = function () {
				swal({title: "El indicador '" + indicator.name + "' fue removido de la actual categoría", type : 'success'});
				$scope.current_indicators.splice($index, 1);
			}

			var handleErrorDelete = function () {
				swal({title: "El indicador no pudo ser removido", type : 'error'});
			}

			ApiService.post_delete_indicator_from_category(params)
					  .then(handleSuccessDelete)
					  .catch(handleErrorDelete);
		}

		swal({
			text : '¿Desea eliminar el indicador "'+ indicator.name + '"?',
			showCancelButton: true,
		    cancelButtonText: 'Cancelar',
			type: 'question'
		}).then(handleSwalActions)
	}

	$scope.reset = function () {
		// reload the category values
		$scope.categories = JSON.parse(JSON.stringify($scope.backup.categories));
		// clean form state
		$scope.formCategories.$setPristine(false);
		// reload the indicators related to the category
		$scope.selectCategory($scope.current_category);
	}

	$scope.save = function () {
		if ($scope.formCategories.$dirty) {

			var handleSwalActions = function (value) {
				if (value) saveCategories();
			}

			swal({
			    title: 'Confirmación',
			    text: '¿Desea actualizar las categorías?',
			    type: 'question',
			    buttons: {
			        cancel: true,
			        confirm: true
			    },
			})
			.then(handleSwalActions);
		}

		if ($scope.formIndicators.$dirty) {
			var handleSwalActions = function (value) {
				if (value) saveIndicators();
			}

			swal({
				title : 'Confirmación',
				text : '¿Desea actualizar los indicadores?',
				type : 'question',
				showCancelButton : true,
			    cancelButtonText: 'Cancelar',
				confirmButtonText: 'Aceptar'
			})
			.then(handleSwalActions);
		}
	}

	$scope.removeCategory = function (category, $index) {

		var handleSwalActions = function (answer) {

			var handleSuccessDelete = function (response) {
				swal({title: "La categoría fue removida", type : 'success'});
				$scope.categories.splice($index, 1);
				$scope.backup.categories = $scope.categories;

				$scope.current_indicators = [];
			}

			var handleErrorDelete = function (err) {
				swal({title: "La categoría no pudo ser removida", type : 'error'});
			}

			if (!answer.value) return;

			ApiService.post_delete_category(category.id)
								.then(handleSuccessDelete)
								.catch(handleErrorDelete);
		}

		swal.mixin();

		swal({text: '¿Desea eliminar la categoría "' + category.name + '"?',
			type: 'question',
		    cancelButtonText: 'Cancelar',
			showCancelButton: true
		 })
		.then(handleSwalActions);
	}

	$scope.openNewCategoryModal = function () {
		var steps = [
		  {

		    text: 'Ingrese el nombre de la categoría',
		    title: 'Introduzca nombre'
		  },
		  {
		    text: 'Ingrese la descripción de la categoría',
		    title: 'Introduzca descripción'
		  }
		]

		var handleSwal = function (answer) {

			if (!answer.value) return;

			var name = answer.value[0];
			var desc = answer.value[1];

			if (!name || !desc) {
				swal({title: "Todos los campos de la categoría son necesarios", type : 'error'});
				return;
			}

			var handleSuccess = function (response) {
				var categoryId = response.data;
				var new_category = {id: categoryId, name : name, qty: 0, amount : 0};

				$scope.categories.push(new_category);
				$scope.backup.categories = $scope.categories;

				swal({title: "Categoría creada exitosamente", type : 'success'});
				$scope.selectCategory(new_category);

				validateCategoriesValues();
			}

			var handleError = function (error) {
				swal({title: "La categoría no pudo ser creada", type : 'error'});
			}

			var params = { name : name, description : desc};
			ApiService.post_create_category(params)
				.then(handleSuccess)
				.catch(handleError);
		}

		swal.mixin({
		  input: 'text',
		  confirmButtonText: 'Siguiente &rarr;',
		  showCancelButton : true,
		  cancelButtonText: 'Cancelar',
		  progressSteps: ['1', '2']
		}).queue(steps).then(handleSwal, function () {
		  swal.resetDefaults()
		})
	}

	/*
	$scope.openNewIndicatorModal = function () {
		swal({
		  title: 'Select Ukraine',
		  input: 'select',
		  inputOptions: {
		    'SRB': 'Serbia',
		    'UKR': 'Ukraine',
		    'HRV': 'Croatia'
		  },
		  inputPlaceholder: 'Select country',
		  buttons : { cancel : true, confirm : true},
		  inputValidator: function (value) {
		    return new Promise(function (resolve, reject) {
		      if (value === 'UKR') {
		        resolve()
		      } else {
		        reject('You need to select Ukraine :)')
		      }
		    })
		  }
		}).then(function (result) {
		  swal({
		    type: 'success',
		    html: 'You selected: ' + result
		  })
		});
	}
	*/

	$scope.selectCategory = function(cat) {
		$scope.loading = true;
		$scope.current_category = cat;

		if (!cat) {
			$scope.current_indicators = [];
			$scope.loading = false;
			return;
		}

		var loadIndicators = function() {

			var handleSuccess = function (response) {
				$scope.current_indicators = response.data;

				$scope.current_indicators.forEach(function (item) {
					item.qty = parseInt(item.qty * 100);
					item.amount = parseInt(item.amount * 100);
				});

				$scope.loading = false;
			}

			var handleError = function (error) {
				swal({title: "Ha ocurrido un inconveniente procesando la solicitud", type : 'error'});
			}

			if (!cat) return;

			ApiService.get_indicator(cat.id)
			.then(handleSuccess)
			.catch(handleError);
		}

		var handleSwalActions = function (answer) {

			var answerYes = function () {
				if ($scope.uncompleteTotal) {
					swal({title : 'Error', text: "Las categorías no pueden guardarse porque su totalidad no presenta el 100%.", type : 'error'})
						.then(function () {
							$scope.$apply(function () {
								$scope.loading = false;
							})
						})
				} else {
					saveCategories();
					loadIndicators();
				}
			}
			var answerNo = function () {
				$scope.reset();
				loadIndicators();
				$scope.loading = false;
			}

			if (answer)
				answerYes()
			else
				answerNo();

			$scope.loading = false;

		}

		// if there are not saved changes in categories list..
		if ($scope.formCategories.$dirty) {
			swal({
				    title: 'Confirmación',
				    type: 'question',
				    text: 'Debe guardar los cambios realizados a las categorías primero, si desea guardar presione Aceptar, de lo contrario las modificaciones serán descartadas.',
				    buttons: {
				        cancel: "Cancelar",
				        confirm: {
				        	text : "Aceptar",
				        	value : true
				        }
				    },
				})
			.then(handleSwalActions);
		} else {
			loadIndicators();
			$scope.loading = false;
		}
	}

	function validateCategoriesValues() {
		if ( $scope.total.categories.amount == 100 && $scope.total.categories.qty == 100) {
			if ($scope.current_indicators.length > 0  && ($scope.total.current_indicators.qty != 100 || $scope.total.current_indicators.amount != 100)) {
				$scope.uncompleteTotal = true;
			} else {
				$scope.uncompleteTotal = false;
			}
		} else {
			$scope.uncompleteTotal = true;
		}
	}

	function setWatch() {
		WATCHABLES_VAR.forEach(function (type) {
			$scope.$watch(type, function (_old, _new) {
				if (_old && _new)
					updateTotal(type);
			}, true);
		})
	}

	function updateTotalAll() {
		WATCHABLES_VAR.forEach(function (type) {
			updateTotal(type);
		});
	}

	function updateTotal(type) {
		$scope.total[type] = { qty : 0, amount : 0};

		$scope[type].forEach((item) => {
			$scope.total[type].qty += (item.qty || 0);
			$scope.total[type].amount += (item.amount || 0);
		});

		validateCategoriesValues();
	}

	function saveCategories() {
		var params = $scope.categories.map(function (category) {
			return {
				id : category.id,
				qty : category.qty / 100,
				amount : category.amount / 100
			}
		});

		var handleSuccess = function () {
			// reset form state (remove dirty field state)
			$scope.formCategories.$setPristine(false);
		}

		var handleError = function (error) {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente procesando la solicitud", type : 'error'});
		}

		ApiService.post_save_categoryweight(params)
			.then(handleSuccess)
			.catch(handleError);
	}

	function saveIndicators() {
		var params = {
			category: $scope.current_category,
			indicators : $scope.current_indicators.map(function (indicator) {
				return {
					id : indicator.id,
					qty : indicator.qty / 100,
					amount : indicator.amount / 100
				}
			})
		}

		var handleSuccess = function () {
			// reset form state (remove dirty field state)
			$scope.formIndicators.$setPristine(false);
		}

		var handleError = function (error) {
			swal("Ha ocurrido un inconveniente :(", error.message, 'error');
		}

		ApiService.post_save_indicatorcategory(params)
			.then(handleSuccess)
			.catch(handleError);
	}

	function loadCategories() {
		var categories_promise = ApiService.get_json_categories_weight();
		// var indicators_promise = ApiService.get_json_indicators();

		var handleSuccess = function (response) {
			$scope.categories = response.data.slice(); // categories
			// $scope.indicators = values[1].data.slice(); // indicators

			$scope.categories.forEach(function (item) {
				item.qty = parseInt(item.qty * 100);
				item.amount = parseInt(item.amount * 100);
			});

			$scope.backup.categories = JSON.parse(JSON.stringify($scope.categories));

			updateTotalAll();
		}

		categories_promise.then(handleSuccess)

		// $q.all([categories_promise, indicators_promise]).then(handleSuccess);
	}

	setWatch();
	loadCategories();
}]);

