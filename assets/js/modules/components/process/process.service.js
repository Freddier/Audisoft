angular.module('ProcessApiService', [])

.service('processApiService.ApiService', function ($http) {
	this.get_root_nodes = function () {
		return $http.get('/procesos/nodes');
	}
	this.get_node = function (node_id) {
		var url = format('/procesos/nodes/{}', node_id);
		return $http.get(url);
	}
	this.get_node_children = function (parent_id) {
		var url = format('/procesos/nodes/children/{}', parent_id);
		return $http.get(url);
	}
	this.get_indicators_per_node = function (node_id) {
		var url = format('/procesos/indicators_per_node/{}', node_id);
		return $http.get(url);
	}
	this.get_top_processes = function (node_type) {
		var url = format('/procesos/top_processes/{}', node_type);
		return $http.get(url);
	}
	this.get_executions_by_indicator = function (indicator_id) {
		var url = format('/procesos/execution_by_indicator/{}', indicator_id);
		return $http.get(url);
	}
})
