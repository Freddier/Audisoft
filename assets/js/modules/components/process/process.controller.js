angular.module('ProcessModule', ['ProcessApiService', 'ConfigModule', 'ProcessDirectives'])

.controller('procesessController', ['$scope', 'processDummy.ApiService', '$timeout', 'sharedDataService.ApiService', function ($scope, ApiService, $timeout, DataService) {
	var ELEMENTS_TO_SHOW = 4;
	$scope.root_nodes = [];
	$scope.deployed_panels = { category : false, macro_process: false, process: false };
	$scope.current_selected = { macro_process : null, process : null };
	$scope.card_lists = { top_processes : {}}
	$scope.awating = false;
    $scope.config = DataService.getConfig();

	// reading event config change
	$scope.move = function (element, movement) {
		var index = $scope[element].index;
		var original = $scope[element].original;

		if (!$scope.is_valid_movement(element, movement)) return;

		if (movement == 'forward' )
			index = $scope[element].index += 1;
		else if (movement == 'backward')
			index = $scope[element].index -= 1;

		// cuz splice alters arrays, clone original node array -> slice(0)
		// take the elements from index to ELEMENTS_TO_SHOW
		$scope[element].list = original.slice(0).splice(index,ELEMENTS_TO_SHOW);
	}

	$scope.is_valid_movement = function (element, movement) {
		/* check if the corresponding arrow movement 'movement' (backward | forward)
		   for the element 'element' (the array of nodes) has the availability to
		   perform a movement
		*/
		if (!$scope[element])
			return false;

		// the current index to handle the nodes offset
		var index = $scope[element].index;
		// the original list of nodes
		var original = $scope[element].original;

		// can move forward
		if (movement == 'forward') {
			// if has the availability to move to next array position
			// [0, 1, showing, showing, showing, showing, 6, 7] -> can go one position forward
			// [0, 1, showing, showing, showing, showing] <- cannot go forward
			if (index < original.length - 4) return true;
		// can move backward
		} else if (movement == 'backward') {
			// if the current index is greater than zero
			// [0, 1, showing, showing, showing, showing] <- can go one position back
			// [showing, showing, showing, showing, 5, 6] <- cannot go back
			if (index > 0) return true;
		} else throw new Error('Movement argument is invalid (backward or forward expected)');

		return false;
	}

	$scope.get_doc = function (execution) {
	    Loading.fadeIn('Espere mientras se descarga su archivo');

	    setTimeout(() => {
		    Loading.fadeOut();
		    document.location = '/procesos/incidences_file/' + execution;
	    }, 700);
	}

	$scope.getTrafficLightColor = function (value) {

		var min_value = 1;
		var max_value = 4;
		var color = '';
		if (min_value < value)
			color = 'green';
		if ( max_value >= value &&  value >= min_value )
			color = 'yellow';
		if (value > max_value)
			color = 'red';

		var light = $(this).find('.traffic-light').find(color);
		light.removeClass(color);
		light.addClass('selected-' + color);
	}


	$scope.executions_by_indicator = function (indicator) {

		var handleSuccess = function (response) {
			indicator.executions = response.data.executions;
		}

		var handleError = function (response) {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente procesando la solicitud", icon : 'error'});
		}

		ApiService.get_executions_by_indicator(indicator.id)
			.then(handleSuccess)
			.catch(handleError);
	}

	$scope.search_nodes_indicators = function (nodeId) {

		var handleSuccess = function (response) {
			var indicators = response.data.indicators;
			$scope.modal_current_indicators = indicators;

            $('#indicators-per-node').modal('show');
		}

		var handleError = function (error) {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente procesando la solicitud", icon : 'error'});
		}

		ApiService.get_example_indicators(nodeId)
			.then(handleSuccess)
			.catch(handleError);
	}

	$scope.search_node_details = function (nodeId) {

		var handleSuccess = function (response) {


			const numberWithCommas = (x) => {
				return function () {
					return x().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					}
			}

			var node = response.data.node;
			node.value_risk = showValueFromIndicatorType(node.risk ,'amount_risk', 'qty_risk');
			node.value_sum = numberWithCommas(showValueFromIndicatorType(node.risk ,'amount_sum', 'qty_sum'));

			$scope.node_details = node;

            $('#process-details').modal('show');
		}

		var handleError = function (error) {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente procesando la solicitud", icon : 'error'});
		}

		ApiService.get_node(nodeId)
			.then(handleSuccess)
			.catch(handleError);
	}

	$scope.show_node_files_modal = function (node ) {

		$scope.selected_node = node;

			    // finally, show modal
	    $('#node-detail-files').modal({
	        dismissible: false
	    });
	}


	$scope.blur = function (identifier, node_id) {
		return $scope.current_selected[identifier] != node_id
									&& $scope.deployed_panels[identifier]
	}

	$scope.getMacroProcesses = function (category_id, element) {

		$(".collapse-macroprocess").collapse('hide');
		$(".collapse-subprocess").collapse('hide');
		$(".collapse-process").collapse('hide');
		$(element).collapse('toggle');

		if ($scope.awating) return;

		$scope.awating = true;

		// // this is for avoing to do a request every time
		// // we click in the panel title

		// Esto esta bien enrredado asi que  va en espanol,
		if ($scope.current_selected.category == category_id) {

			$scope.current_selected.process = null;
			if ($scope.deployed_panels.category) {
				$scope.deployed_panels.category = false;
				return;
			} else {
				$scope.deployed_panels.category = true;
				$scope.current_selected.category = category_id;
			}
		} else {
			$scope.current_selected.category = category_id;
			$scope.deployed_panels.category = true;
		}

		ApiService.get_node_children(category_id)
			.catch(handleError)
			.then(handleSuccess('macroprocesses_nodes'));
		$(element).collapse('toggle');
	}

	$scope.getProcess = function (macroprocess, element) {


		if ($scope.awating || !macroprocess.is_enabled) return;

		$scope.awating = true;

		var macroprocess_id = macroprocess.id;

		if ($scope.current_selected.macro_process == macroprocess_id) {
			$(".collapse-subprocess").collapse('hide');
			$(".collapse-process").collapse('hide');
		} else {
			$scope.current_selected.macro_process = macroprocess_id;
			$scope.deployed_panels.macro_process = true;
		}

		ApiService.get_node_children(macroprocess_id)
			.catch(handleError)
			.then(handleSuccess('process_nodes'));
		
		$(element).collapse('toggle');
	}

	$scope.getSubProcess = function (process, element) {

		if ($scope.awating || !process.is_enabled) return;

		$scope.awating = true;

		$(element).collapse('toggle');

		var process_id = process.id;

		if (!$scope.deployed_panels.process) {
			$scope.deployed_panels.process = true;

			if ($scope.current_selected.process != process_id) {
				$scope.current_selected.process = process_id;
				$scope.deployed_panels.process = true;


				ApiService.get_node_children(process_id)
					.catch(handleError)
					.then(handleSuccess('subprocess_nodes'));
			}
		} else {
			$scope.deployed_panels.process = false;
			$scope.current_selected.process = undefined;
		}
	}


	var handleSuccess = function (destine) {
		return function (response) {
			var nodes = response.data.children_nodes;

			nodes.forEach(function (node) {
				node.value_risk = showValueFromIndicatorType(node.risk, 'amount_risk', 'qty_risk');
				node.value_sum = showValueFromIndicatorType(node.risk, 'amount_sum', 'qty_sum');
			});

			$scope[destine] = {};
			$scope[destine].list = nodes.slice(0).splice(0, ELEMENTS_TO_SHOW);
			$scope[destine].original = nodes;
			$scope[destine].index = 0;
		}
	}

	var handleError = function (error) {
		swal({title : "Ha ocurrido un inconveniente procesando la solicitud", content: error, icon : 'error'});
	}

	var showValueFromIndicatorType = function (obj, key_amount, key_qty) {
		// define what happens if indicator type is qty or amount.
		// In case of being qty it takes the value of key_amount,
		// Same for key_qty

		return function() {
			var indicator_type = $scope.config.indicator_type;
			var value_field = null;

			if (indicator_type == 'amount')
				value_field = key_amount;

			if (indicator_type == 'qty')
				value_field = key_qty;

			var value = obj[value_field];

			if (indicator_type == 'qty')
				value = parseInt(value);

			return obj ? value : 0;
		}
	}

	var enable_context_menu = function () {

		new BootstrapMenu('.btn-enabled', {
		    fetchElementData: function($rowElem) {
		        return $rowElem.data('nodeId');
		    },
		    actions: [{
		        name: 'Ver detalles',
		        onClick: function(nodeId) {
		        	$scope.search_node_details(nodeId);
		        }
		    }, {
		        name: 'Ver indicadores',
		        onClick: function(nodeId) {
		        	$scope.search_nodes_indicators(nodeId);
		        }
		    }]
		});

		new BootstrapMenu('.btn-disabled', {
		    fetchElementData: function($rowElem) {
		        return $rowElem.data('nodeId');
		    },
		    actions: [{
		        name: 'Objecto deshabilitado',
		    }]
		});
	}

	var wait_for_collapse = function () {
		$('.collapse').on('shown.bs.collapse hidden.bs.collapse', function () {
			$scope.awating = false;
			$scope.$apply();
		})
	}

	var get_top_process_risk = function (node_type) {

		var handleSuccess = function (response) {
			$scope.card_lists.top_processes[node_type] = response.data.top_processes[node_type];
			$scope.card_lists.top_processes[node_type].forEach(function (item) {
				item.value_risk = showValueFromIndicatorType(item, 'amount_risk', 'qty_risk');
	            // item.risk_level = universal.getLevel(item.value_risk());
			});
		}

		ApiService.get_top_processes(node_type)
			.then(handleSuccess)
			.catch(handleError)
	}

	enable_context_menu();
	wait_for_collapse();

	get_top_process_risk('MAC_PROC');
	get_top_process_risk('MID_PROC');
	get_top_process_risk('SUB_PROC');


	(function () {
		ApiService.get_graph_history()
		.then(function (res) {
			$scope.graph_history = res.data;
		})
	})();

}]);
