angular.module('ProcessDummyService', [])

.service('processDummy.ApiService', function ($http, $q) {
	this.get_promise = function (data) {
		var result = {
			data : data
		}

		var defer = $q.defer();
		defer.resolve(result);
		return defer.promise;
	}
	this.get_example_indicators = function (node_id) {
					// {code : 'DAC-BR-PR-A20', name : 'Préstamos LD Sin Fórmula Capital e Interés', executions : [{'load_date' :' 10/11/2017',  file : 2}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
		var indicators = {
				7 : [
					{code : 'DAC-BR-PR-A05', name : 'Préstamos Garantía Certificado PXP', executions : [{"file": 1, "load_date": "2017-10-05"}, {"file": 2, "load_date": "2016-05-09"}, {"file": 3, "load_date": "2016-05-08"}] },
					{code : 'DAC-BR-PR-53', name : 'Préstamos LD no desembolso en la cuenta del cliente', executions : [{"file": 9, "load_date": "2014-12-15"}, {"file": 10, "load_date": "2015-08-03"}, {"file": 11, "load_date": "2015-12-01"} ]},
				],
				181 : [
					// {code : 'DAC-BR-PR-A11', name : 'Préstamos Vehículo Sin Garantía Prendarias', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-BR-TC-264', name : 'TC que exceden 80% Monto Garantizado', executions : [{"file": 20, "load_date": "2016-01-28"}] },
					// {code : 'DAC-BR-TC-272', name : 'TC Garantizadas Activas con Cuotas Vencidas', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
				],
				15 : [
					{code : 'DAC-BR-PR-A05', name : 'Préstamos Garantía Certificado PXP', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
				],
				95 : [
					{code : 'DAC-BR-PR-22', name : 'Préstamos Hipotecarios Sin Póliza Vida o Incendio', executions : [{"file": 7, "load_date": "2017-05-05"}, {"file": 8, "load_date": "2017-07-19"}] },
				],
				6 : [
					{code : 'DAC-BR-PR-22', name : 'Préstamos Hipotecarios Sin Póliza Vida o Incendio', executions : [{"file": 7, "load_date": "2017-05-05"}, {"file": 8, "load_date": "2017-07-19"}] },
					{code : 'DAC-BR-PR-A05', name : 'Préstamos Garantía Certificado PXP', executions : [{"file": 1, "load_date": "2017-10-05"}, {"file": 2, "load_date": "2016-05-09"}, {"file": 3, "load_date": "2016-05-08"}] },
					// {code : 'DAC-BR-PR-A20', name : 'Préstamos LD Sin Fórmula Capital e Interés', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-BR-PR-53', name : 'Préstamos LD no desembolso en la cuenta del cliente', executions : [{"file": 9, "load_date": "2014-12-15"}, {"file": 10, "load_date": "2015-08-03"}, {"file": 11, "load_date": "2015-12-01"} ] },
				],
				180	 : [
					{code : 'DAC-BR-PR-A11', name : 'Préstamos Vehículo Sin Garantía Prendarias', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-BR-TC-264', name : 'TC que exceden 80% Monto Garantizado', executions : [{"file": 20, "load_date": "2016-01-28"}] },
				],
				185 : [
					{code : 'DAC-BR-TC-354', name : 'Avance efectivo TC no reflejados Cadencie', executions : [{"file": 23, "load_date": "2017-03-13"}, {"file": 24, "load_date": "2017-02-24"}, {"file": 25, "load_date": "2017-01-02"}] },
					{code : 'DAC-BR-TC-316', name : 'Avance Credimás vía Netbanking no Reflejados en CADENCIE', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					// {code : 'DAC-BR-TC-272', name : 'TC Garantizadas Activas con Cuotas Vencidas', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
				],
				164 : [
					{code : 'DAC-BR-PR-A11', name : 'Préstamos Vehículo Sin Garantía Prendarias', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-BR-TC-264', name : 'TC que exceden 80% Monto Garantizado', executions : [{"file": 20, "load_date": "2016-01-28"}] },
					{code : 'DAC-BR-TC-354', name : 'Avance efectivo TC no reflejados Cadencie', executions : [{"file": 23, "load_date": "2017-03-13"}, {"file": 24, "load_date": "2017-02-24"}, {"file": 25, "load_date": "2017-01-02"}] },
					{code : 'DAC-BR-TC-316', name : 'Avance Credimás vía Netbanking no Reflejados en CADENCIE', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					// {code : 'DAC-BR-TC-272', name : 'TC Garantizadas Activas con Cuotas Vencidas', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
				],
				427 : [
					{code : 'DAC-BR-TC-400', name : 'Comisiones no Cobradas en TC', executions : [{"file": 21, "load_date": "2017-05-19"}] },
					{code : 'DAC-BR-TC-398', name : 'Cargos Duplicados en Tarjetas de Crédito', executions : [{"file": 22, "load_date": "2017-03-16"}] },
					// {code : 'DAC-BR-CD-70', name : 'Incorrecta Penalización de Certificados', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					// {code : 'DAC-BR-AH-CC-432', name : 'Cobro incorrecto Comisión por Balance Mínimo', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					// {code : 'DAC-BR-AH-CC-422', name : 'Cobro incorrecto comisión 180 días sin movimientos', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ]}
				],
				418 : [
					{code : 'DAC-BR-TC-400', name : 'Comisiones no Cobradas en TC', executions : [{"file": 21, "load_date": "2017-05-19"}] },
					{code : 'DAC-BR-TC-398', name : 'Cargos Duplicados en Tarjetas de Crédito', executions : [{"file": 22, "load_date": "2017-03-16"}] },
					// {code : 'DAC-BR-CD-70', name : 'Incorrecta Penalización de Certificados', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					// {code : 'DAC-BR-AH-CC-432', name : 'Cobro incorrecto Comisión por Balance Mínimo', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					// {code : 'DAC-BR-AH-CC-422', name : 'Cobro incorrecto comisión 180 días sin movimientos', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ]}
				],
				151 : [
					{code : 'DAC-MAY-11-17', name : 'Tarjeta de Crédito con total disponibilidad consumido en 15 Días', executions : [{"file": 29, "load_date": "2017-07-21"}, {"file": 30, "load_date": "2017-07-03"}] },
					{code : 'DAC-MAY-10-17', name : 'Tarjetas castigadas por ejecutivo del último año', executions : [{"file": 31, "load_date": "2017-06-21"}] },
				],
				138 : [
					{code : 'DAC-MAY-11-17', name : 'Tarjeta de Crédito con total disponibilidad consumido en 15 Días', executions : [{"file": 29, "load_date": "2017-07-21"}, {"file": 30, "load_date": "2017-07-03"}] },
					{code : 'DAC-MAY-10-17', name : 'Tarjetas castigadas por ejecutivo del último año', executions : [{"file": 31, "load_date": "2017-06-21"}] },
				],
				559 : [
					{code : 'DAC-BR-AH-CC-A02', name : 'Cuentas Activas Más 3-10 Años sin Movimiento', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-BR-AH-CC-207', name : 'Comisiones después de tres años sin actividad', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
				],
				558 : [
					{code : 'DAC-BR-AH-CC-A02', name : 'Cuentas Activas Más 3-10 Años sin Movimiento', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-BR-AH-CC-207', name : 'Comisiones después de tres años sin actividad', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
				],
				616 : [
					{code : 'DAC-BR-FR-201', name : 'Transacciones de cuentas ociosas a cuentas personales', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-MAY-10-17', name : 'Tarjetas castigadas por ejecutivo del último año', executions : [{"file": 31, "load_date": "2017-06-21"}] },
				],
				596 : [
					// {code : 'DAC-BR-FR-201', name : 'Transacciones de cuentas ociosas a cuentas personales', executions : [{'load_date' :' 10/11/2017',  file : 1}, {'load_date' : '17/11/2017',  file : 2}, {'load_date' : '12/02/2018',  file : 3}, {'load_date' : '24/03/2018',  file : 4} ] },
					{code : 'DAC-MAY-10-17', name : 'Tarjetas castigadas por ejecutivo del último año', executions : [{"file": 31, "load_date": "2017-06-21"}] },
				],
				136 : [
					{code : 'DAC-DIC-12-15', name : 'Fallecidos en la JCE con movimientos en la cuenta del banco', executions : [ {"file": 37, "load_date": "2017-10-10"}, {"file": 38, "load_date": "2017-10-19"}, {"file": 39, "load_date": "2017-11-03"}] },
					{code : 'DAC-FEB-03-17', name : 'Cuentas Inactivas Reactivadas', executions : [{"file": 35, "load_date": "2017-10-03"}, {"file": 36, "load_date": "2017-02-06"}] },
				],
				135 : [
					{code : 'DAC-DIC-12-15', name : 'Fallecidos en la JCE con movimientos en la cuenta del banco', executions : [ {"file": 37, "load_date": "2017-10-10"}, {"file": 38, "load_date": "2017-10-19"}, {"file": 39, "load_date": "2017-11-03"}] },
					{code : 'DAC-FEB-03-17', name : 'Cuentas Inactivas Reactivadas', executions : [{"file": 35, "load_date": "2017-10-03"}, {"file": 36, "load_date": "2017-02-06"}] },
				]
			}[node_id]
		return this.get_promise({indicators: indicators});
	}
	this.get_indicators = function () {
		var result = [
			{
				"id": 1,
				"code": "DAC-BR-PR-A15",
				"name": "Préstamos sumatoria garantía no cumple con las políticas",
				"description": "Identificar préstamos de vehículos cancelados o saldados cuya póliza de seguro aún esté activa."
			},
			{
				"id": 2,
				"code": "DAC-BR-PR-A05",
				"name": "Préstamos Garantía Certificado PXP",
				"description": "Verificar si existen préstamos del tipo PXP (con garantía de certificados), cuyas tasas de interés estén por debajo de lo establecido por el CAP."
			},
			{
				"id": 3,
				"code": "DAC-BR-PR-A11",
				"name": "Préstamos Vehículo Sin Garantía Prendarias",
				"description": "Verificar si existen préstamos de vehículos cuyas garantías sean prendarias y no estén a nombre del deudor."
			},
			{
				"id": 4,
				"code": "DAC-BR-PR-A20",
				"name": "Préstamos LD Sin Fórmula Capital e Interés",
				"description": "Verificar si existen préstamos bajo el límite discrecional otorgados sin la fórmula de pago Capital e Interés."
			},
			{
				"id": 5,
				"code": "DAC-BR-PR-53",
				"name": "Préstamos LD no desembolso en la cuenta del cliente",
				"description": "Préstamos LD no desembolso en la cuenta del cliente."
			},
			{
				"id": 6,
				"code": "DAC-BR-PR-22",
				"name": "Préstamos Hipotecarios Sin Póliza Vida o Incendio",
				"description": "Identificar los Préstamos hipotecarios que no tengan póliza de vida o incendio."
			},
			{
				"id": 7,
				"code": "DAC-BR-TC-264",
				"name": "TC que exceden 80% Monto Garantizado",
				"description": "Identificar las Tarjetas de Crédito con garantía de depósitos que exceden en más de un 80% el monto de la garantía."
			},
			{
				"id": 8,
				"code": "DAC-BR-TC-400",
				"name": "Comisiones no Cobradas en TC",
				"description": "Identifica las tarjetas a las cuales no les ha sido cobrado el cargo por emisión. "
			},
			{
				"id": 9,
				"code": "DAC-BR-TC-398",
				"name": "Cargos Duplicados en Tarjetas de Crédito",
				"description": "Identifica los cargos por emisión y mantenimiento que fueron cargados varias veces al cliente"
			},
			{
				"id": 10,
				"code": "DAC-BR-TC-354",
				"name": "Avance efectivo TC no reflejados Cadencie",
				"description": "Identifica los avances de efectivo realizados a través de netbanking y que no fueron aplicados en CADENCIE."
			},
			{
				"id": 11,
				"code": "DAC-BR-TC-316",
				"name": "Avance Credimás vía Netbanking no Reflejados en CADENCIE",
				"description": "Identifica los credimás realizados a través de netbanking que no fueron registrados en CADENCIE."
			},
			{
				"id": 12,
				"code": "DAC-BR-TC-272",
				"name": "TC Garantizadas Activas con Cuotas Vencidas",
				"description": "Identifica las tarjetas de crédito otorgadas con garantías que tienen cuotas vencidas."
			},
			{
				"id": 13,
				"code": "DAC-MAY-11-17",
				"name": "Tarjeta de Crédito con total disponibilidad consumido en 15 Días",
				"description": "Evalúa las tarjetas colocadas, cuyo comportamiento exhibe  indicios de fraudes."
			},
			{
				"id": 14,
				"code": "DAC-MAY-10-17",
				"name": "Tarjetas castigadas por ejecutivo del último año",
				"description": "Realiza un análisis de los gestores que tienen un alto % de colocaciones castigadas."
			},
			{
				"id": 15,
				"code": "DAC-BR-AH-CC-A02",
				"name": "Cuentas Activas Más 3-10 Años sin Movimiento",
				"description": "Identificar si se existen cuentas de ahorro activas sin ejecutar ningún movimiento durante un transcurso de 3 años o inactivas que no han sido transferidas al Banco central luego de tener 10 años sin movimientos."
			},
			{
				"id": 16,
				"code": "DAC-BR-AH-CC-207",
				"name": "Comisiones después de tres años sin actividad",
				"description": "Identificar transacciones por concepto de comisiones a cuentas que tengan más de tres años de inactividad."
			},
			{
				"id": 17,
				"code": "DAC-BR-CD-70",
				"name": "Incorrecta Penalización de Certificados",
				"description": "Identificar las penalizaciones que se han realizado de forma incorrecto."
			},
			{
				"id": 18,
				"code": "DAC-BR-AH-CC-432",
				"name": "Cobro incorrecto Comisión por Balance Mínimo",
				"description": "Identifica las cuentas de ahorros y corrientes a las cuales se les realizaron cargos por balance mínimo no acorde a lo establecido en el tarifario."
			},
			{
				"id": 19,
				"code": "DAC-BR-AH-CC-422",
				"name": "Cobro incorrecto comisión 180 días sin movimientos",
				"description": "Identifica las comisiones que son cobradas a las cuentas inactivas con más de 180 días en incumplimiento a lo establecido por la superintendencia de Bancos."
			},
			{
				"id": 20,
				"code": "DAC-DIC-12-15",
				"name": "Fallecidos en la JCE con movimientos en la cuenta del banco",
				"description": "Identifican los clientes reportados como fallecidos en la JCE a los cuales se les están realizando movimientos."
			},
			{
				"id": 21,
				"code": "DAC-BR-FR-201",
				"name": "Transacciones de cuentas ociosas a cuentas personales",
				"description": "Identifica transacciones realizadas desde cuentas con mucho tiempo de inactividad a cuentas de empleados."
			},
			{
				"id": 22,
				"code": "DAC-JUL-01-17",
				"name": "Fraude Cobro de Comisión Retiros por Caja Menores de 10 Mil",
				"description": "Identifica los cajeros que cobran las comisiones por retiros menores de 10 mil y la depositan en su cuenta."
			},
			{
				"id": 25,
				"code": "DAC-ABR-01-17",
				"name": "Préstamos con desembolso a cta. y retiro por ATM",
				"description": "Identificar los préstamos con desembolsos a cuentas cuyo monto desembolsado haya sido retirado en un 50% o un 100% a traves de cajeros automáticos (ATM)."
			},
			{
				"id": 26,
				"code": "DAC-ENE-08-17",
				"name": "Empleados con desvinculación Cuenta Debitar Préstamo",
				"description": "Empleados con desvinculación Cuenta Debitar  Préstamo"
			},
			{
				"id": 27,
				"code": "DAC-FEB-03-17",
				"name": "Cuentas Inactivas Reactivadas",
				"description": "Reporte de la Cuentas Inactivas y/o Abandonada, Activadas y Reactivadas"
			}
		];
		return this.get_promise(result);
	}
	this.get_node = function (node_id) {
		var url = format('/procesos/nodes/{}', node_id);
		return $http.get(url);
	}
	this.get_node_children = function (parent_id) {
		var url = format('/procesos/nodes/children/{}', parent_id);
		return $http.get(url);
	}
	this.search_nodes_indicators = function (parent_id) {
		var result = {
			2 : {
				"node": {
				"id": 6,
				"title": "Gesti\u00f3n Comercial",
				"parent": 2,
				"is_enabled": true,
				"node_type": "MAC_PROC",
				"description": "Este Macroproceso tiene como prop\u00f3sito gestionar procesos de comercializaci\u00f3n de productos y servicios financieros con el objetivo de cumplir con las metas comerciales del Banco, siendo capaz de dise\u00f1ar y evaluar estrategias de ventas, apoy\u00e1ndose en las herramientas tecnol\u00f3gicas y recursos que permitan un mejor desempe\u00f1o y alto rendimiento. ",
				"leader_name": "NO_ASIGNADO",
				"leader_position": "",
				"owner_name": "William Read",
				"owner_position": "",
				"number_of_children": 6,
				"risk": {
					"qty_risk": 2.0,
					"amount_risk": 7.0,
					"qty_sum": 2564,
					"amount_sum": 2391200.2,
					"executions_qty": 4
					}
				}
			}
		}
		return this.get_promise(result);
	}
	this.get_indicators_per_node = function (node_id) {
		var url = format('/procesos/indicators_per_node/{}', node_id);
		return $http.get(url);
	}
	this.get_top_processes = function (node_type) {
		var data = {
			"top_processes": {
			  "MAC_PROC": [
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Gestión Comercial"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [  {"file": 9, "date": "2014-12-15"}, {"file": 10, "date": "2015-08-03"}, {"file": 11, "date": "2015-12-01"} ] }
				  ],
			      "amount_risk": 21.5,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 26.2,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Gestión de Captación"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [  {"file": 9, "date": "2014-12-15"}, {"file": 10, "date": "2015-08-03"}, {"file": 11, "date": "2015-12-01"} ] }
				  ],
			      "amount_risk": 19.8,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 19.2,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Gestión de Riesgos"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [[  {"file": 9, "date": "2014-12-15"}, {"file": 10, "date": "2015-08-03"}, {"file": 11, "date": "2015-12-01"} ]] }
				  ],
			      "amount_risk": 14.9,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 17.1,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Gestión de colocación"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 11.0,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 9.8,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 7,
			        "is_enabled": true,
			        "leader_name": "Capn Crunch Cereal",
			        "leader_position": "ABC Entertainment",
			        "node_type": "MID_PROC",
			        "owner_name": "SunChips Multigrain",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 6,
			        "title": "Gestión Operativa"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A15' , name : 'Préstamos sumatoria garantía no cumple con las políticas' , runs : [[{'date' :' 10/01/2018',  file : 1}, {'date' : '17/01/2018',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ], {'date' : '01/01/2017',  file : 2}, {'date' : '01/01/2017',  file : 3}, {'date' : '01/01/2017',  file : 4}]},
					{ code: 'DAC-BR-PR-A05' , name : 'Préstamos Garantía Certificado PXP', runs : [{'date' :' 10/01/2018',  file : 1}, {'date' : '15/02/2018',  file : 2}, {'date' : '09/02/2018',  file : 3}, {'date' : '14/02/2018',  file : 4} ] }
				  ],
			      "amount_risk": 4.6,
			      "amount_sum": 214.2,
			      "executions_qty": 4,
			      "id": 1,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 7,
			      "qty_risk": 4.2,
			      "qty_sum": 2564
			    }
			  ],
			  "MID_PROC": [
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Venta"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A05' , name : 'Préstamos Garantía Certificado PXP'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 22.2,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 19.4,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Formalización operativa"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 20.2,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 18.7,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Matenimiento de Pasivas"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 11.8,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 15.6,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Riesgo de Crédito"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 7.7,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 3.1,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 7,
			        "is_enabled": true,
			        "leader_name": "Capn Crunch Cereal",
			        "leader_position": "ABC Entertainment",
			        "node_type": "MID_PROC",
			        "owner_name": "SunChips Multigrain",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 6,
			        "title": "Administración de Activas"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A15' , name : 'Préstamos sumatoria garantía no cumple con las políticas' , runs : [[{'date' :' 10/01/2018',  file : 1}, {'date' : '17/01/2018',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ], {'date' : '01/01/2017',  file : 2}, {'date' : '01/01/2017',  file : 3}, {'date' : '01/01/2017',  file : 4}]},
					{ code: 'DAC-BR-PR-A05' , name : 'Préstamos Garantía Certificado PXP', runs : [{'date' :' 10/01/2018',  file : 1}, {'date' : '15/02/2018',  file : 2}, {'date' : '09/02/2018',  file : 3}, {'date' : '14/02/2018',  file : 4} ] }
				  ],
			      "amount_risk": 2.0,
			      "amount_sum": 214.2,
			      "executions_qty": 4,
			      "id": 1,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 7,
			      "qty_risk": 1.0,
			      "qty_sum": 2564
			    }
			  ],
			  "SUB_PROC": [
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Negociación Excepción de Tasa"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 13.6,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 9.7,
			      "qty_sum": 2564
			    },
			    {
			      "node": {
			        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis semper metus, vel sagittis orci eleifend eu. Donec placerat molestie imperdiet. Etiam faucibus arcu vitae nulla feugiat, id interdum tellus aliquet. Integer vitae condimentum erat. Donec turpis lectus, scelerisque in sagittis id, euismod id velit. Pellentesque elit felis, rhoncus in eros non, maximus cursus mi. Morbi molestie aliquet nisi, sed laoreet ex aliquet et. Duis ornare ligula et est malesuada, nec luctus massa laoreet. Pellentesque neque elit, tempor vel urna sed, efficitur porttitor velit.",
			        "id": 12,
			        "is_enabled": true,
			        "leader_name": "Seattles Best Coffee",
			        "leader_position": "Disney Vacation",
			        "node_type": "MID_PROC",
			        "owner_name": "Baked! Doritos Tortilla Chips",
			        "owner_position": "Disney–ABC Television",
			        "parent_id": 5,
			        "title": "Constitución de garantías"
			      },
			      indicators : [
					{ code: 'DAC-BR-PR-A20' , name : 'Préstamos LD Sin Fórmula Capital e Interés'  ,runs : [{'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/02/2018',  file : 3}, {'date' : '24/03/2018',  file : 4} ]},
					{ code: 'DAC-BR-PR-53' , name : 'Préstamos LD no desembolso en la cuenta del cliente' ,runs : [ {'date' :' 10/11/2017',  file : 1}, {'date' : '17/11/2017',  file : 2}, {'date' : '12/12/2017',  file : 3}, {'date' : '09/01/2018',  file : 4} ] }
				  ],
			      "amount_risk": 7.7,
			      "amount_sum": 170057.2,
			      "executions_qty": 4,
			      "id": 5,
			      "last_update": "2018-03-22T18:34:33Z",
			      "node_id": 12,
			      "qty_risk": 5.4,
			      "qty_sum": 2564
			    }
			  ],
			}
		}
		return this.get_promise(data);
	}
	this.get_graph_history = function () {
		var result = {
			MAC_PROC : [
			{
				"id": 6,
				"title": "Gestión Comercial",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					35.00,
					547.00,
					230.00
				]
			},
			{
				"id": 8,
				"title": "Gestión de colocación",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					5412.00,
					8951.00,
					9841.00
				]
			},
			{
				"id": 9,
				"title": "Oferta de productos o servicios",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					24.00,
					1644500.00,
					458000.00
				]
			},
			{
				"id": 12,
				"title": "Gestión Operativa",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn "
				],
				"values": [
					2510.00,
					9447.00
				]
			}
		],
			SUB_PROC : [
			{
				"id": 6,
				"title": "Negociación Excepción de Tasa",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					35.00,
					547.00,
					230.00
				]
			},
			{
				"id": 8,
				"title": "Constitución de garantías",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					5412.00,
					8951.00,
					9841.00
				]
			},
		],
			MID_PROC : [
			{
				"id": 6,
				"title": "Seguridad Transaccional",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					35.00,
					547.00,
					230.00
				]
			},
			{
				"id": 8,
				"title": "Aseguramiento del cumplimiento Regulatorio",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					5412.00,
					8951.00,
					9841.00
				]
			},
			{
				"id": 9,
				"title": "Administración de Recaudos por comisiones",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					24.00,
					1644500.00,
					458000.00
				]
			},
			{
				"id": 9,
				"title": "Administracón de garantías",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					24.00,
					1644500.00,
					458000.00
				]
			},
			{
				"id": 9,
				"title": "Riesgo de Crédito",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					24.00,
					1644500.00,
					458000.00
				]
			},
			{
				"id": 9,
				"title": "Matenimiento de Pasivas",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn ",
					"Antepenultima Ejecuciòn"
				],
				"values": [
					24.00,
					1644500.00,
					458000.00
				]
			},
			{
				"id": 12,
				"title": "Administración de Activas",
				"columns": [
					"Ùltima Ejecuciòn",
					"Penultima Ejecuciòn "
				],
				"values": [
					2510.00,
					9447.00
				]
			}
		],
		}
		return this.get_promise(result);
	}
})
