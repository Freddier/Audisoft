angular.module('ProcessDirectives', [])

.directive('trafficLightComponent', function() {
  return {
    restrict : 'C',
    scope : {
      config : '=',
      node : '='
    },
    controller : function ($scope) {

        $scope.$watch('config', function(){

        var node = $scope.node;
        var min_value = Number($scope.config.risk_percent_low);
        var max_value = Number($scope.config.risk_percent_medium);
        var value = node.value_risk();

        ['green', 'yellow', 'red'].forEach(function(color){
            $scope[color] = '';
        });

        if (min_value > value)
            $scope.green = 'selected';
        if ( max_value >= value &&  value >= min_value )
            $scope.yellow = 'selected';
        if (value > max_value)
            $scope.red = 'selected';

      }, true);
    },
    template : '\
          <div class="traffic-light"> \
              <div class="light red [[red]]-red"></div> \
              <div class="light yellow [[yellow]]-yellow"></div> \
              <div class="light green [[green]]-green"></div> \
          </div>'
  }
})

.directive('topProcessComponent', function() {
    return {
        restrict: 'E',
        scope: {
            config: '=',
            modalEvent: '=',
            list: '=',
            title: '@'
        },
        controller: function($scope) {

            $scope.getLevel = function(value) {
                var low_value = loaded_config.risk_percent[0];
                var medium_value = loaded_config.risk_percent[1];
                values = {
                    low: low_value,
                    medium: medium_value,
                    high: 100
                };
                for (var level in values) {
                    if (value <= values[level])
                        return level;
                }
            }

        },
        templateUrl: '/static/js/modules/components/process/templates/top-process.html'
    }
})

.directive('selectpicker', function($timeout) {
  return {
    restrict : 'C',
    link : function ($scope, $element) {
        // $timeout(function() {$element.selectpicker();}, 500);
    }

  }
})

.directive('graphicComponent', ['processDummy.ApiService',function(ApiService) {
  return {
    restrict : 'E',
    scope : {
      config : '=',
      source : '=',
      type : '@',
      title : '@'
    },
    controller : function ($scope, $element) {

      $scope.model = '';

      $scope.load_chart = function (item) {
          var setChart = function(data) {
              // create new chartjs ans save instance in scope
              var canvas = $element.find('canvas');
              $scope.charts = new Chart(canvas, {
                  type: 'bar',
                  data: {
                      labels: data.columns.reverse(),
                      datasets: [{
                          label: "",
                          backgroundColor: universal.getColors(3),
                          data: data.values.reverse()
                      }]
                  },
                  options: {
                      legend: {
                          display: false
                      },
                      title: {
                          display: false
                      },
                      scales: {
                          yAxes: [{
                              ticks: {
                                  beginAtZero: true
                              }
                          }]
                      }
                  }
              });
          };
          var handleSuccess = function (response) {
            //console.log
                var history_data;
                var item_id;

                var data = response.data[$scope.type];

                if (!$scope.model) {


                  item_id = data[0].id;
                  $scope.model = item_id;
                  //Setting first value a default
                  setTimeout(function(){
                      $('.process-picker').trigger('change');
                  }, 200);
                } else
                  item_id = $scope.model.id;

                //$scope.model = 8;
                //console.log($scope.model);
                data.filter(function (item) {
                  if (item.id == item_id) history_data = item;
                });

                if ($scope.charts) {
                    // if saved graph instance, update it
                    $scope.charts.data.datasets[0].data = history_data.values.reverse();
                    $scope.charts.data.labels = history_data.columns.reverse();
                    $scope.charts.update();
                } else {
                    // if not saved graph instance, create it
                    setChart(history_data);
                }
            }

          ApiService.get_graph_history()
              .then(handleSuccess);
        }

      $scope.load_chart();
    },
    templateUrl : '/static/js/modules/components/process/templates/chart_history_by_process.html'
  }
}])
