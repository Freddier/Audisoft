angular.module('ConfigModule', ['rzModule'])

// rzModule -> dependency for range slider

.controller('configController', ['$scope', 'sharedDataService.ApiService', '$rootScope' ,function(scope, ApiService, rootScope){
	scope.changing_config = false;
	scope.config = {};
	scope.config.considered_executions = parseInt(loaded_config.considered_executions);
	scope.config.risk_percent_low = loaded_config.risk_percent[0];
	scope.config.risk_percent_medium = loaded_config.risk_percent[1];
	scope.config.indicator_type = loaded_config.indicator_type;

	ApiService.saveConfig(scope.config);

	var toggleMenu = function(rightPosition){
		$('ul#chat-out').animate({
			right: rightPosition + '%'
		}, 200);
	}

	$('#config-button').on('click', function(){
		toggleMenu('0');
	});

	$('body').on('click', '#sidenav-overlay, a.chat-close-collapse.right', function(){
		toggleMenu('-105');
	});

	// AMOUNT = TRUE
	// QTY = FALSE

	scope.toogleIndicatorType = function () {
		scope.boolean_indicator_type = !scope.boolean_indicator_type;
		getIndicatorValue();

		ApiService.saveConfig(scope.config);
		rootScope.$broadcast('config_has_changed');
	}

	var getIndicatorValue = function () {
		scope.config.indicator_type = scope.boolean_indicator_type ? 'amount': 'qty';
	}

	var translateValue = function (expr) {
		return (expr == 'amount') ? true : (expr == 'qty') ? false : new Error('Invalid Indicador Type');
	}

	var setIndicatorValue = function (booleanValue) {
		if (booleanValue)
			return 'amount';
		else
			return 'qty';
	}

	scope.boolean_indicator_type = translateValue(scope.config.indicator_type);

	scope.changeConfig = function(){
		if(scope.changing_config) return false;

		if(!scope.config.considered_executions || scope.config.considered_executions < 1){
			alert('Debe especificar la cantidad de ejecuciones')
			return false;
		}

		if(!scope.config.risk_percent_low){
			alert('Debe especificar el porcentaje de riesgo inferior')
			return false;
		}

		if(!scope.config.risk_percent_medium){
			alert('Debe especificar el porcentaje de riesgo medio')
			return false;
		}

		if(scope.config.risk_percent_medium < scope.config.risk_percent_low){
			alert('El porcentaje de riesgo medio no puede ser inferior o igual al porcentaje de riesgo bajo')
			return false;
		}

		var config = {
			risk_percent: JSON.stringify([scope.config.risk_percent_low, scope.config.risk_percent_medium]),
			indicator_type : rootScope.indicator_type,
			considered_executions: scope.config.considered_executions
		};

		scope.changing_config = true;


		var handleSuccessResponse = function (res) {
			risk_percents = [scope.config.risk_percent_low, scope.config.risk_percent_medium];

			ApiService.saveConfig(scope.config);

			rootScope.$broadcast('config_has_changed');
			scope.changing_config = false;
		}

		var handleFailedResponse = function () {
			swal({title : "Opps", content: "Ocurrió un problema al intentar guardar la configuración", icon : 'error'});
		}

		ApiService.change_config(config)
		.then(handleSuccessResponse)
		.catch(handleFailedResponse);
	}
}])

