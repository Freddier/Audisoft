angular.module('ReportModule', [])

.controller('reportsController', ['$scope', 'ReportApiService.ApiService', function(scope, ApiService){
	scope.report_data = {};

	var getData = function(){

		var handleSuccessResponse = function () { console.log(res); }
		var handleFailedResponse  = function () {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente generando el repote", type : 'error'});
		}

		ApiService.get_executions(scope.report_data)
		.then(handleSuccessResponse)
		.catch(handleFailedResponse)
	}

	scope.showReport = function(){
		var showModal = function(columns, data){
			swal({
			  width:'90%',
			  html: '<div class="table-container"><table id="table_data" class="display" width="100%"></table></div><div class="col s12 buttons"><button id="download_pdf" class="btn waves-effect waves-light cyan lighten-1" type="button" name="action" style="margin-right: 10px;"><img src="/static/images/system/loading.gif" style="width: 22px;margin-bottom: -5px;margin-right: 5px; display:none"> PDF</button><button id="cancel" class="btn waves-effect waves-light teal white-text darken-2" type="button" name="reset">Cerrar</button></div>',
			  showCloseButton: true,
			  showCancelButton: false,
			  showConfirmButton: false,
			  focusConfirm: false,
			  confirmButtonText:'Descargar PDF',
			  cancelButtonText: 'Cancelar',
			  onOpen: function(){
		  	    var setTableInfo = function(columns, data){
		            $('#table_data').DataTable( {
		                data: data,
		                "scrollX": true,
		                columns: columns
		            } );
		        }

		        setTableInfo(columns, data);
			  }
			});
		}

		var handleSuccessResponse = function (response) {
			var columns = response.data.execution_data.columns;
			var data = response.data.execution_data.data;
			showModal(columns, data);
		}
		var handleFailedResponse = function (response) {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente generando el repote", type : 'error'});
		}

		ApiService.get_executions(scope.report_data)
		.then(handleSuccessResponse)
		.catch(handleFailedResponse)

	}

	scope.setInitialValues = function(){
		scope.report_data = {
			report_name:'',
			report_type:'no-graph',
			offices:[],
			indicators:[],
			categories:[],
			executions_count:'1',
			indicator_type: 'count',
			group_by:''
		};

		$('#offices, #indicators, #categories').val([]);
		setTimeout(function() {
			$('div.generate select, div.basic select').material_select();
			$('select#indicators, select#categories, select#offices').bootstrapDualListbox('refresh', true);
		}, 10);
	}

	scope.reset = function(){
		swal({ text : '¿Realmente desea reiniciar los valores del reporte?', showCancelButton : true})
		.then(function () {
			scope.$apply(function(){
				scope.setInitialValues();
			});
		}, function () {})
	}

	$('body').on('click', '#download_pdf', function(){
		var image = $(this).find('img');
		image.css('display', 'inherit');

		var handleSucess = function (response) {
			window.open('/static/reports/' + response.data.report_name);
		}

		var handleCatch = function (response) {
			swal({title : "Opps", content: "Ha ocurrido un inconveniente generando el repote", icon : 'error'});
		}

		var handleFinally = function (response) {
			image.css('display', 'none');
		}

		ApiService.download_pdf(scope.report_data)
		.then(handleSucess)
		.catch(handleCatch)
		.finally(handleFinally)
	});

	$('body').on('click', '#cancel', function(){
		swal.close();
	});

	scope.setInitialValues();
}])

