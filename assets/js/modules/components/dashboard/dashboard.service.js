angular.module('DashboardApiService', [])

.service('dashboardApiService.ApiService', function ($http) {
	this.get_history = function (entity_type, entity_id) {
		var url = format('/dashboard/offices/api/get_history/{}/{}', entity_type, entity_id);
		return $http.get(url)
	}
	this.get_offices_risk = function () {
		return $http.get('/dashboard/offices/api/offices_risk')
	}
})
