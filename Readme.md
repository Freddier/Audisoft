# AudiSoft

### Consideraciones
Una vez se le clone el repositorio se debe crear y activar el entorno virtual

```sh
$ virtualenv env
$ env\Scripts\activate.bat
```
Y proceder a instalar las dependencias necesarias de python.
```sh
(env)$ pip install -r requirements.txt
```
Y de Javacript
```sh
(env)$ cd assets
(env)$ npm install
```
Recordar que cuando se realice una actualizaciòn en alguna dependencia del **package.json** se debe realizar un `(env)$ npm update` dentro del folder *assets/*

