# add/extend django features

from collections import namedtuple
from datetime import datetime, time, date
from mimetypes import guess_type
from decimal import Decimal

from django.db import connections
from django.http import HttpResponse, FileResponse
from django.shortcuts import render as render_page
from django.db import models

from numpy import int64
from simplejson import dumps as json_encode
import logging

from AudISoft.strings import Strings
from apps.security.auth_backend import AuthenticationBackend

class JsonResponse(HttpResponse):
    "Custom json encoder for non native objects"

    def default_json_encoder(self, obj):
        if isinstance(obj, datetime):
            r = obj.isoformat()
            if obj.microsecond:
                r = r[:23] + r[26:]
            if r.endswith('+00:00'):
                r = r[:-6] + 'Z'
            return r
        elif isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, time):
            if is_aware(obj):
                raise ValueError("JSON can't represent timezone-aware times.")
            r = obj.isoformat()
            if obj.microsecond:
                r = r[:12]
            return r
        elif isinstance(obj, int64):
            return int(obj)
        elif isinstance(obj, Decimal):
            return int(obj)
        else:
            try:
                dict_ = obj.__dict__
                dict_.pop('_state', None)
                return dict_
            except Exception:
                raise TypeError(repr(obj) + ' is not JSON serializable')

    def __init__(self, data, safe=True, **kwargs):
        if safe and not isinstance(data, dict):
            raise TypeError('In order to allow non-dict objects to be '
                            'serialized set the safe parameter to False')
        kwargs.setdefault('content_type', 'application/json')
        data = json_encode(data, default=self.default_json_encoder)
        super(JsonResponse, self).__init__(content=data, **kwargs)


class FileResponse(FileResponse):
    # Custom file response class
    # By default, django FileResponse class is not able to
    # set directyle the filename and default content type is
    # text/html

    def __init__(self, data, filename, **kwargs):
        content_type = guess_type(filename)
        super().__init__(data,**kwargs)
        self['Content-Disposition'] = 'attachment; filename="%s"' % filename


def log_exception(source, exception, message):
    logger = logging.getLogger(source)
    logger.error('')
    logger.error('# # # # # # # # #  <EXCEPTION> # # # # # # # # # ')
    logger.error(message)
    logger.error(exception)
    logger.error('# # # # # # # # #  </EXCEPTION> # # # # # # # # # ')


def execute_sql(sql):
    connection = connections['default']
    with connection.cursor() as cursor:
        cursor.execute(sql)


def dictfetchall(sql):
    "Perfrom custom sql select "
    connection = connections['default']
    with connection.cursor() as cursor:
        cursor.execute(sql)
        # Return all rows from a cursor as a dict
        columns = [col[0] for col in cursor.description]

        for row in cursor.fetchall():
            yield dict(zip(columns, row))


def namedtuplefetchall(sql):
    "Return all rows from a cursor as a namedtuple"
    connection = connections['default']
    with connection.cursor() as cursor:
        cursor.execute(sql)
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])

        for row in cursor.fetchall():
            r = []
            for i, value in enumerate(row):
                # cast to float in case of Decimal
                r.append(float(row[i]) if isinstance(value, Decimal) else value)
            yield nt_result(*r)


def render(request, view, page_data=None):
    "return a rendered page with session data loaded"

    if not page_data:
        page_data = {}

    if not page_data.get('config'):
        AuthenticationBackend.set_default_session(request)

    page_data['config'] = request.session[Strings.Setting.USER_SETTINGS]
    return render_page(request, view, page_data)


def select_fields_from_query(data, fields):
    # extract named fields from an orm query select

    ls = []
    for item in data:
        row = {}
        for field in fields:
            row[field] = item.__dict__[field]
        ls.append(row)
    return ls


class EnumField(models.Field):
    """
    A field class that maps to MySQL's ENUM type.

    Usage:

    class Card(models.Model):
        suit = EnumField(values=('Clubs', 'Diamonds', 'Spades', 'Hearts'))

    c = Card()
    c.suit = 'Clubs'
    c.save()
    """

    def __init__(self, *args, **kwargs):
        self.values = ( v[0] for v in kwargs['choices'])
        super().__init__(*args, **kwargs)

    def db_type(self, connection):
        if connection.settings_dict['ENGINE'] == 'django.db.backends.mysql':
            return "enum({0})".format( ','.join("'%s'" % v for v in self.values) )
        else:
            raise EnvironmentError('This Enum field only work for MYSQL engine')

