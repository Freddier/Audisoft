from django.contrib import messages
from AudISoft.strings import Strings


class SessionTimeoutMiddleware:
    # This middleware is used to check if user session has expired
    # and redirect to login page with a notification.

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        LOGIN_PATH = '/login'
        # cleaning the array
        map(None, messages.get_messages(request))

        if request.path != LOGIN_PATH \
                and not request.user.is_authenticated:
            messages.warning(request, Strings.Session.EXPIRED,
                             extra_tags=Strings.MessageType.NOTIFICATION)

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
