# Define all strings used in project


class Strings:

    class Setting:
        EXECUTIONS_QTY = 'considered_executions'
        USER_SETTINGS = 'USER_SETTINGS'
        INDICATOR_TYPE = 'indicator_type'

    class Session:
        BAD_LOGIN = 'Las credenciales son incorrectas'
        ADMIN_NOT_PERMITTED = 'Pica debe ser utilizado por un usuario NO administrador'
        EXPIRED = 'Su sesión ha expirado'

    class ContentType:
        ZIP = "application/zip, application/octet-stream"

    class Response:
        GENERAL_BAD_RESPONSE = 'Bad Request'

    class MessageType:
        NOTIFICATION = 'notification'

    class IndicatorType:
        AMOUNT = 'amount'
        QTY = 'qty'
