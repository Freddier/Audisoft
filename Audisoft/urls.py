"""AudISoft URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from decorator_include import decorator_include

from django.contrib import admin
from django.contrib.auth.decorators import login_required
#     user_passes_test, permission_required
from django.views.generic import RedirectView
from django.urls import path

from apps.security import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='/dashboard/offices')),

    # authentication
    path('login/', auth_views.login, name='login'),
    path('logout/', auth_views.logout, name='logout'),

    path('dashboard/offices/',
         decorator_include([login_required], 'apps.dashboard.offices.urls'),
         name='offices'),

    path('dashboard/processes/',
         decorator_include([login_required], 'apps.dashboard.processes.urls'),
         name='processes'),

    path('indicator/',
         decorator_include([login_required], 'apps.indicator.urls'),
         name='indicator'),

    path('mantenimientos/',
         decorator_include([login_required], 'apps.cruds.urls'),
         name='cruds'),

    path('security/',
         decorator_include([login_required], 'apps.security.urls'),
         name='security'),

    # path(r'^procesos/',
    #         include('processes.urls'),
    #         wrap=(permission_required('admin.access_processes_dashboard',  login_url='/logout'), login_required),
    #         name='processes'),

    # path(r'^mantenimientos/',
    #         include('cruds.urls'),
    #         wrap=(permission_required('admin.access_maintenance',  login_url='/logout'), login_required),
    #         name='cruds'),
]
