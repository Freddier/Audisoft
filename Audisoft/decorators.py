'''
    Define project global decorators

    Author: Freddier
'''
from django.shortcuts import redirect


def mainpage_if_logged(func):
    def func_wrapper(request):
        MAIN_PAGE = 'dashboard'
        if request.user.is_authenticated:
            return redirect(MAIN_PAGE)
        else:
            return func(request)
    return func_wrapper


def validate_indicator_type(func):
    def func_wrapper(*args, **kwargs):
        if kwargs.get('indicator_type', 'qty') not in ('qty', 'amount'):
            raise Exception(f" '{kwargs['indicator_type']}' is not a valid indicator type."
                            "\nExpected 'qty' or 'amount'.")
        return func(*args, **kwargs)
    return func_wrapper
